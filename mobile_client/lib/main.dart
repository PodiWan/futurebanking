import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:futurebanking/src/blocs/user/user_bloc.dart';
import 'package:futurebanking/src/ui/screens/dashboard_page.dart';
import 'package:futurebanking/src/ui/screens/login_page.dart';
import 'package:futurebanking/src/utils/routes.dart';

void main() {
  runApp(const Dashboard());
}

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserBloc>(
      create: (context) => UserBloc(),
      child: MaterialApp(
        title: 'Future Banking',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: const ColorScheme(
              primary: Color.fromARGB(255, 255, 255, 255),
              primaryVariant: Color.fromARGB(255, 44, 113, 235),
              secondary: Color.fromARGB(255, 125, 135, 161),
              secondaryVariant: Color.fromARGB(255, 197, 197, 197),
              surface: Color.fromARGB(240, 240, 240, 252),
              background: Color.fromARGB(255, 252, 252, 252),
              error: Color.fromARGB(255, 255, 32, 30),
              onPrimary: Color.fromARGB(255, 36, 37, 45),
              onSecondary: Color.fromARGB(255, 103, 105, 109),
              onSurface: Color.fromARGB(255, 252, 252, 252),
              onBackground: Color.fromARGB(255, 252, 252, 252),
              onError: Color.fromARGB(255, 255, 32, 30),
              brightness: Brightness.dark),
          cardColor: const Color.fromARGB(255, 44, 113, 235),
          textTheme: const TextTheme(
            bodyText1: TextStyle(),
            bodyText2: TextStyle(),
            headline1: TextStyle(),
            subtitle1: TextStyle(),
          ).apply(
            bodyColor: const Color.fromARGB(255, 36, 37, 45),
            displayColor: const Color.fromARGB(255, 36, 37, 45),
          ),
        ),
        darkTheme: ThemeData(
          colorScheme: const ColorScheme(
              primary: Color.fromARGB(255, 36, 37, 45),
              primaryVariant: Color.fromARGB(255, 44, 113, 235),
              secondary: Color.fromARGB(255, 255, 255, 255),
              secondaryVariant: Color.fromARGB(255, 103, 105, 109),
              surface: Color.fromARGB(255, 55, 58, 68),
              background: Color.fromARGB(255, 55, 58, 68),
              error: Color.fromARGB(255, 255, 32, 30),
              onPrimary: Color.fromARGB(255, 36, 37, 45),
              onSecondary: Color.fromARGB(255, 103, 105, 109),
              onSurface: Color.fromARGB(255, 55, 58, 68),
              onBackground: Color.fromARGB(255, 55, 58, 68),
              onError: Color.fromARGB(255, 255, 32, 30),
              brightness: Brightness.dark),
          cardColor: const Color.fromARGB(255, 44, 113, 235),
        ),
        home: const LoginPage(),
        routes: {
          Routes.rootLogin: (context) => LoginPage(),
          Routes.rootDashboard: (context) => DashboardPage(),
        },
      ),
    );
  }
}
