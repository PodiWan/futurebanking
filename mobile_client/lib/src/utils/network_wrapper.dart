import 'dart:convert';

import 'package:futurebanking/src/config/config_api.dart';
import 'package:http/http.dart' as http;

class NetworkWrapper {
  NetworkWrapper();

  static Future<http.Response> get({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  }) {
    //TODO verify JWT token before call
    return http.get(
      Uri.http(
        Config.API_URL,
        path,
        queryParameters,
      ),
      headers: headers,
    );
  }

  static Future<http.Response> post({
    required String path,
    required Map<String, dynamic> body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  }) {
    //TODO verify JWT token before call
    return http.post(
      Uri.http(
        Config.API_URL,
        path,
        queryParameters,
      ),
      headers: headers,
      body: jsonEncode(body),
    );
  }

  static Future<http.Response> put({
    required String path,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  }) {
    //TODO verify JWT token before call
    return http.put(
      Uri.http(
        Config.API_URL,
        path,
        queryParameters,
      ),
      headers: headers,
      body: jsonEncode(body),
    );
  }
}
