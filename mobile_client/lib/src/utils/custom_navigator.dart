import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomPageRouteBuilder<T> extends PageRouteBuilder<T> {
  CustomPageRouteBuilder({required this.screen})
      : super(
          pageBuilder: (_, __, ___) => screen,
          transitionDuration: const Duration(milliseconds: 225),
          reverseTransitionDuration: const Duration(milliseconds: 225),
          transitionsBuilder: (_, animation, ___, child) {
            const begin = Offset(1.0, 0.0);
            const end = Offset.zero;
            final tween = Tween(begin: begin, end: end);
            final offsetAnimation = animation.drive(tween);

            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
        );

  final Widget screen;
}
