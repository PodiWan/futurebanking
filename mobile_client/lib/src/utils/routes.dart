class Routes {
  Routes._();

  static const String rootNavigator = '/';
  static const String rootTransactionsNavigator = '/transactions';
  static const String sendMoneyTransactions = '/transactions/send';
  static const String rootLogin = '/login';
  static const String rootDashboard = '/dashboard';
}
