import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:futurebanking/src/blocs/user/user_bloc.dart';
import 'package:futurebanking/src/models/card_information.dart';

class CardListDisplay extends StatelessWidget {
  const CardListDisplay({Key? key, required this.cards}) : super(key: key);

  final List<CardInformation> cards;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 240,
      child: PageView.builder(
        itemCount: cards.length,
        itemBuilder: (BuildContext context, int index) {
          return CardDisplay(card: cards[index]);
        },
        onPageChanged: (int index) =>
            BlocProvider.of<UserBloc>(context).add(UpdatePaymentsEvent(cardId: cards[index].id)),
      ),
    );
  }
}

class CardDisplay extends StatelessWidget {
  const CardDisplay({Key? key, required this.card}) : super(key: key);

  final CardInformation card;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: InkWell(
        splashColor: Theme.of(context).colorScheme.secondaryVariant,
        onTap: () {},
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.6),
                spreadRadius: -5,
                blurRadius: 16,
                offset: const Offset(3, 3),
              ),
            ],
          ),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(16.0)),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                padding: const EdgeInsets.all(1.0),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Colors.white.withOpacity(0.8),
                      Colors.black.withOpacity(0.2),
                    ],
                  ),
                ),
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(16.0)),
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.blue,
                        Colors.deepPurple,
                      ],
                    ),
                  ),
                  child: Align(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width - 32,
                      child: ListTile(
                        title: Text(card.number.substring(0, 4) +
                            " " +
                            card.number.substring(4, 8) +
                            " " +
                            card.number.substring(8, 12) +
                            " " +
                            card.number.substring(12, 16)),
                        trailing: Text(
                          card.holderName.toUpperCase(),
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(card.expiryDate.month.toString() +
                            "/" +
                            card.expiryDate.year.toString()),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
