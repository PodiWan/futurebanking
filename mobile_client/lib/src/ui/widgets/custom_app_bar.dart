import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTopAppBar {
  static AppBar buildAppBar(
      {required BuildContext context,
      required String title,
      VoidCallback? onBackButtonPress,
      Widget? actionButton}) {
    return AppBar(
      elevation: 0.0,
      toolbarHeight: 64.0,
      centerTitle: true,
      title: Text(title),
      backgroundColor: Theme.of(context).colorScheme.background,
      titleTextStyle: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).colorScheme.secondary),
      systemOverlayStyle: const SystemUiOverlayStyle(
        systemNavigationBarColor: Color(0xFF000000),
        statusBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.light,
      ),
      leading: (onBackButtonPress != null)
          ? IconButton(
              splashRadius: 16,
              iconSize: 16,
              onPressed: onBackButtonPress,
              icon: Icon(
                Icons.arrow_back_ios_new_rounded,
                color: Theme.of(context).colorScheme.secondary,
              ),
            )
          : null,
      actions: [
        if (actionButton != null) ...[actionButton]
      ],
    );
  }
}
