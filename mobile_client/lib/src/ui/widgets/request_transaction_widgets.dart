import 'package:flutter/material.dart';
import 'package:futurebanking/src/utils/routes.dart';

class RequestTransaction extends StatelessWidget {
  const RequestTransaction({Key? key, required this.optionText, required this.route})
      : super(key: key);

  final String optionText;
  final String route;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 64,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(48.0),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed(route);
        },
        child: Align(
          child: Text(
            optionText,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontSize: 16,
            ),
          ),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}

class RequestTransactionOptions extends StatelessWidget {
  const RequestTransactionOptions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 200,
      child: DecoratedBox(
        child: ListView(
          children: const [
            RequestTransaction(
                key: Key('SendMoney'),
                optionText: 'Send Money',
                route: Routes.sendMoneyTransactions),
          ],
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
        ),
      ),
    );
  }
}
