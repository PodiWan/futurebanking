import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:futurebanking/src/blocs/user/user_bloc.dart';
import 'package:futurebanking/src/models/payment_information.dart';

class TransactionsScrollableSheet extends StatefulWidget {
  const TransactionsScrollableSheet({Key? key}) : super(key: key);

  @override
  State<TransactionsScrollableSheet> createState() => _TransactionsScrollableSheetState();
}

class _TransactionsScrollableSheetState extends State<TransactionsScrollableSheet> {
  late final String currentDate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    DateTime date = DateTime(now.year, now.month, now.day);
    currentDate = date.day.toString() + "-" + date.month.toString() + "-" + date.year.toString();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        return DraggableScrollableSheet(
          snap: true,
          initialChildSize: 0.49,
          minChildSize: 0.49,
          snapSizes: const [0.49, 1.0],
          builder: (BuildContext context, ScrollController scrollController) {
            var radius = const Radius.circular(30.0);
            return ClipRRect(
              borderRadius: BorderRadius.only(topLeft: radius, topRight: radius),
              child: Container(
                padding: EdgeInsets.only(left: 4.0, right: 8.0),
                color: Theme.of(context).colorScheme.primary,
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 24.0, bottom: 24.0),
                        child: ListTile(
                          title: const Text(
                            'Today',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                          subtitle: Text(
                            currentDate,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        controller: scrollController,
                        itemCount: (state as AuthenticatedUserState).currentPayments.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ExpensesListTile(
                              paymentInformation:
                              (state as AuthenticatedUserState).currentPayments[index]);
                        },
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}

class ExpensesListTile extends StatelessWidget {
  const ExpensesListTile({Key? key, required this.paymentInformation}) : super(key: key);

  final PaymentInformation paymentInformation;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        radius: 30.0,
        backgroundImage: NetworkImage(paymentInformation.sender.avatarUrl),
        backgroundColor: Colors.transparent,
      ),
      title: Text(paymentInformation.sender.firstName + " " + paymentInformation.sender.lastName),
      subtitle: Text(paymentInformation.transferType),
      trailing: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Text(
              paymentInformation.amount.toString() + " " + paymentInformation.currency,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 6.0),
            child: Text(
              paymentInformation.cardType.toString(),
              style: const TextStyle(color: Colors.grey),
            ),
          )
        ],
      ),
    );
  }
}
