import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:futurebanking/src/blocs/payment/payment_bloc.dart';
import 'package:futurebanking/src/blocs/user/user_bloc.dart';
import 'package:futurebanking/src/models/reciever.dart';
import 'package:futurebanking/src/models/transaction_information.dart';
import 'package:futurebanking/src/services/account_service.dart';
import 'package:futurebanking/src/ui/widgets/custom_app_bar.dart';

class SendMoneyScreen extends StatelessWidget {
  const SendMoneyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PaymentBloc>(
      create: (context) => PaymentBloc()
        ..add(FetchRecievers(
            myId: (BlocProvider.of<UserBloc>(context).state as AuthenticatedUserState).user.id)),
      child: SendMoneyScreenUI(),
    );
  }
}

List<String> paymentsTypes = ["Transfer", "Rental", "RestaurantAndCafes", "Sports"];

class SendMoneyScreenUI extends StatelessWidget {
  SendMoneyScreenUI({Key? key}) : super(key: key);

  TextEditingController amountController = TextEditingController();
  Reciever? reciever;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: CustomTopAppBar.buildAppBar(
        context: context,
        title: "Send Money",
        onBackButtonPress: () => Navigator.of(context).pop(),
      ),
      body: BlocBuilder<PaymentBloc, PaymentState>(
        builder: (context, state) {
          if (state is PaymentInitial) {
            return Center(
              child: CircularProgressIndicator(
                color: Theme.of(context).colorScheme.secondary,
              ),
            );
          }
          if (state is PaymentData) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.01234567890, horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      DropdownButton(
                        value: (state as PaymentData).paymentType,
                        style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                        items: paymentsTypes.map(
                          (String items) {
                            return DropdownMenuItem(
                              value: items,
                              child: Text(items),
                            );
                          },
                        ).toList(),
                        onChanged: (String? value) {
                          BlocProvider.of<PaymentBloc>(context)
                              .add(PaymentTypeChooseEvent(paymentType: value ?? "Transfer"));
                        },
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 2.5,
                        child: TextField(
                          maxLength: 7,
                          textAlign: TextAlign.end,
                          style: TextStyle(fontSize: 20.0),
                          textAlignVertical: TextAlignVertical.center,
                          controller: amountController,
                          keyboardType: TextInputType.text,
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          decoration: InputDecoration(
                            suffix: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text("RON"),
                            ),
                            suffixStyle: TextStyle(fontSize: 24.0),
                            border: InputBorder.none,
                            hintText: '0',
                            counterText: "",
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: state.recievers.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: CircleAvatar(
                          radius: 30.0,
                          backgroundImage: NetworkImage(state.recievers[index].avatarUrl),
                          backgroundColor: Colors.transparent,
                        ),
                        title: Text(state.recievers[index].firstName +
                            " " +
                            state.recievers[index].lastName),
                        subtitle: Text(state.recievers[index].email),
                        trailing: Radio<Reciever>(
                          value: state.recievers[index],
                          groupValue: state.recipient,
                          onChanged: (Reciever? value) {
                            BlocProvider.of<PaymentBloc>(context)
                                .add(PaymentRecipientChooseEvent(recipient: value));
                          },
                        ),
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size.fromHeight(56.0),
                    ),
                    onPressed: () async {
                      await AccountService.sendMoneyRequest(
                        body: TransactionInformation(
                          senderCardId:
                              (BlocProvider.of<UserBloc>(context).state as AuthenticatedUserState)
                                  .currentCard
                                  .id,
                          receiverCardId: state.recipient.cardId,
                          day: DateTime.now(),
                          amount: int.parse(amountController.value.text),
                          currency: 3,
                          transferType: paymentsTypes.indexOf(state.paymentType),
                        ),
                      );
                      BlocProvider.of<UserBloc>(context).add(
                        RefreshPaymentsEvent(
                          cardId:
                              (BlocProvider.of<UserBloc>(context).state as AuthenticatedUserState)
                                  .currentCard
                                  .id,
                        ),
                      );
                      Navigator.of(context).pop();
                    },
                    child: Align(
                      child: Text(
                        "Send",
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.secondary,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      alignment: Alignment.center,
                    ),
                  ),
                ),
              ],
            );
          }
          return SizedBox();
        },
      ),
    );
  }
}
