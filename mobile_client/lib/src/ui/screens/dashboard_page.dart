import 'package:flutter/material.dart';
import 'package:futurebanking/src/ui/navigators/transactions_navigator.dart';
import 'package:futurebanking/src/ui/screens/mortgages_screen.dart';
import 'package:futurebanking/src/ui/screens/profile_screen.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPage();
}

class _DashboardPage extends State<DashboardPage> {
  int _activeScreenIndex = 0;

  void _onItemTapped(int index) {
    setState(() {});
    _activeScreenIndex = index;
  }

  static const List<Widget> _screens = <Widget>[
    TransactionsNavigator(key: Key('transactions_screen')),
    MortgagesScreen(key: Key('mortgages_screen')),
    ProfileScreen(key: Key('profile_screen'))
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.surface,
      body: SafeArea(child: _screens.elementAt(_activeScreenIndex)),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.account_balance_wallet), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.account_balance), label: 'Mortgages'),
          BottomNavigationBarItem(icon: Icon(Icons.account_circle), label: 'Profile'),
        ],
        currentIndex: _activeScreenIndex,
        onTap: _onItemTapped,
        backgroundColor: Theme.of(context).colorScheme.primary,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        unselectedItemColor: Theme.of(context).colorScheme.secondaryVariant,
      ),
    );
  }
}
