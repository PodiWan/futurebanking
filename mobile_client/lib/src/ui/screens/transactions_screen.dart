import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:futurebanking/src/blocs/user/user_bloc.dart';
import 'package:futurebanking/src/ui/widgets/card_widgets.dart';
import 'package:futurebanking/src/ui/widgets/request_transaction_widgets.dart';
import 'package:futurebanking/src/ui/widgets/transaction_widgets.dart';

class TransactionsScreen extends StatefulWidget {
  const TransactionsScreen({Key? key}) : super(key: key);

  @override
  State<TransactionsScreen> createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (!state.isUserAuthenticated) {
          return const SizedBox(
            child: Text("You shall not be here!!!"),
          );
        }
        return Stack(
          children: [
            Column(
              children: [
                CardListDisplay(cards: (state as AuthenticatedUserState).cards),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  child: RequestTransactionOptions(),
                ),
              ],
            ),
            const TransactionsScrollableSheet(),
          ],
        );
      },
    );
  }
}
