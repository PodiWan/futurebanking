import 'package:flutter/material.dart';
import 'package:futurebanking/src/ui/screens/send_money_screen.dart';
import 'package:futurebanking/src/ui/screens/transactions_screen.dart';
import 'package:futurebanking/src/utils/custom_navigator.dart';
import 'package:futurebanking/src/utils/routes.dart';

GlobalKey<NavigatorState> profileNavigatorKey = GlobalKey<NavigatorState>();

class TransactionsNavigator extends StatelessWidget {
  const TransactionsNavigator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: profileNavigatorKey,
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case Routes.rootNavigator:
            return CustomPageRouteBuilder(
              screen: const TransactionsScreen(),
            );
          case Routes.sendMoneyTransactions:
            return CustomPageRouteBuilder(
              screen: SendMoneyScreen(),
            );
        }
      },
    );
  }
}
