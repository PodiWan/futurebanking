part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

class UserLogin extends UserEvent {
  const UserLogin({required this.onSuccess, required this.email, required this.password});

  final String email;
  final String password;
  final Function() onSuccess;

  @override
  List<Object?> get props => [email, password, onSuccess];
}

class UpdatePaymentsEvent extends UserEvent {
  const UpdatePaymentsEvent({required this.cardId});

  final String cardId;

  @override
  List<Object?> get props => [cardId];
}

class RefreshPaymentsEvent extends UserEvent {
  const RefreshPaymentsEvent({required this.cardId});

  final String cardId;

  @override
  List<Object?> get props => [cardId];
}

class UserLogout extends UserEvent {
  @override
  List<Object?> get props => throw UnimplementedError();
}
