part of 'user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState(this.isUserAuthenticated);
  final bool isUserAuthenticated;
}

class UnauthenticatedUserState extends UserState {
  UnauthenticatedUserState() : super(false);

  @override
  List<Object> get props => [];
}

class AuthenticatedUserState extends UserState {
  AuthenticatedUserState({
    required this.currentCard,
    required this.payments,
    required this.user,
    required this.cards,
    required this.currentPayments,
  }) : super(true);

  final User user;
  final List<CardInformation> cards;
  final CardInformation currentCard;
  final Map<String, List<PaymentInformation>> payments;
  final List<PaymentInformation> currentPayments;

  AuthenticatedUserState copyWith({
    User? user,
    List<CardInformation>? cards,
    CardInformation? currentCard,
    Map<String, List<PaymentInformation>>? payments,
    List<PaymentInformation>? currentPayments,
  }) {
    return AuthenticatedUserState(
      user: user ?? this.user,
      cards: cards ?? this.cards,
      payments: payments ?? this.payments,
      currentPayments: currentPayments ?? this.currentPayments,
      currentCard: currentCard ?? this.currentCard,
    );
  }

  @override
  List<Object> get props => [user, cards, payments, currentPayments, currentCard];
}
