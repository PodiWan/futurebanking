import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:futurebanking/src/models/card_information.dart';
import 'package:futurebanking/src/models/payment_information.dart';
import 'package:futurebanking/src/models/user.dart';
import 'package:futurebanking/src/services/account_service.dart';
import 'package:futurebanking/src/services/user_service.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UnauthenticatedUserState()) {
    on<UserLogin>((UserLogin event, Emitter<UserState> emit) => _loginUser(event, emit));
    on<UpdatePaymentsEvent>((UpdatePaymentsEvent event, Emitter<UserState> emit) =>
        _updateCurrentPayments(event, emit));
    on<RefreshPaymentsEvent>((RefreshPaymentsEvent event, Emitter<UserState> emit) =>
        _refreshCurrentPayments(event, emit));
  }

  Future<void> _loginUser(UserLogin event, Emitter<UserState> emit) async {
    User user = await UserService.authenticateUser(email: event.email, password: event.password);
    if (user != const User.initial()) {
      List<CardInformation> cards = await AccountService.getCardsByUserId(userId: user.id);
      List<Map<String, List<PaymentInformation>>> payments = await Future.wait(
        cards.map(
          (card) async => {
            card.id: await AccountService.getPaimentsByCardId(cardId: card.id),
          },
        ),
      );
      Map<String, List<PaymentInformation>> paymentsMap =
          payments.reduce((map1, map2) => map1..addAll(map2));
      emit(
        AuthenticatedUserState(
          user: user,
          cards: cards,
          payments: paymentsMap,
          currentPayments: paymentsMap[cards[0].id] ?? [],
          currentCard: cards[0],
        ),
      );
      event.onSuccess();
    } else {
      emit(UnauthenticatedUserState());
    }
  }

  _updateCurrentPayments(UpdatePaymentsEvent event, Emitter<UserState> emit) {
    AuthenticatedUserState prevState = (state as AuthenticatedUserState);
    emit(
      prevState.copyWith(
        currentPayments: prevState.payments[event.cardId],
        currentCard: prevState.cards.firstWhere((card) => card.id == event.cardId),
      ),
    );
  }

  _refreshCurrentPayments(RefreshPaymentsEvent event, Emitter<UserState> emit) async {
    List<PaymentInformation> payments =
        await AccountService.getPaimentsByCardId(cardId: event.cardId);
    AuthenticatedUserState prevState = (state as AuthenticatedUserState);
    emit(
      prevState.copyWith(
        payments: {...prevState.payments, event.cardId: payments},
        currentPayments: payments,
        currentCard: prevState.cards.firstWhere((card) => card.id == event.cardId),
      ),
    );
  }
}
