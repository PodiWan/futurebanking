part of 'payment_bloc.dart';

abstract class PaymentState extends Equatable {
  const PaymentState();
}

class PaymentInitial extends PaymentState {
  @override
  List<Object?> get props => [];
}

class PaymentData extends PaymentState {
  PaymentData(
      {required this.recievers,
      required this.recipient,
      required this.amount,
      required this.paymentType});

  final String paymentType;
  final String amount;
  final Reciever recipient;
  final List<Reciever> recievers;

  PaymentData copyWith({
    Reciever? recipient,
    String? amount,
    String? paymentType,
    List<Reciever>? recievers,
  }) {
    return PaymentData(
        recipient: recipient ?? this.recipient,
        amount: amount ?? this.amount,
        paymentType: paymentType ?? this.paymentType,
        recievers: recievers ?? this.recievers);
  }

  @override
  List<Object> get props => [paymentType, amount, recipient, recievers];
}
