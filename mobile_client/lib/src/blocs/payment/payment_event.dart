part of 'payment_bloc.dart';

abstract class PaymentEvent extends Equatable {
  const PaymentEvent();
}

class FetchRecievers extends PaymentEvent {
  FetchRecievers({required this.myId});

  final String myId;

  @override
  List<Object?> get props => [myId];
}

class PaymentTypeChooseEvent extends PaymentEvent {
  PaymentTypeChooseEvent({required this.paymentType});

  final String paymentType;

  @override
  List<Object?> get props => [paymentType];
}

class PaymentRecipientChooseEvent extends PaymentEvent {
  PaymentRecipientChooseEvent({required this.recipient});

  final Reciever? recipient;

  @override
  List<Object?> get props => [recipient];
}

class PaymentFinishEvent extends PaymentEvent {
  PaymentFinishEvent({required this.amount});

  final String amount;

  @override
  List<Object?> get props => [amount];
}
