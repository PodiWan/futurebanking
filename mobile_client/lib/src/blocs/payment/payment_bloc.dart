import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:futurebanking/src/models/reciever.dart';
import 'package:futurebanking/src/services/account_service.dart';

part 'payment_event.dart';
part 'payment_state.dart';

class PaymentBloc extends Bloc<PaymentEvent, PaymentState> {
  PaymentBloc() : super(PaymentInitial()) {
    on<FetchRecievers>((event, emit) => _fetchRecievers(event, emit));
    on<PaymentTypeChooseEvent>(
      (event, emit) => emit(
        (state as PaymentData).copyWith(paymentType: event.paymentType),
      ),
    );
    on<PaymentRecipientChooseEvent>(
      (event, emit) => emit(
        (state as PaymentData).copyWith(recipient: event.recipient),
      ),
    );
    on<PaymentFinishEvent>(
      (event, emit) => emit(
        (state as PaymentData).copyWith(amount: event.amount),
      ),
    );
  }

  _fetchRecievers(FetchRecievers event, Emitter<PaymentState> emit) async {
    List<Reciever> recievers = await AccountService.getRecievers();
    recievers = recievers.where((element) => element.id != event.myId).toList();
    emit(
      PaymentData(
        amount: '',
        recipient: Reciever(
            id: "", firstName: "", lastName: "", email: "", avatarUrl: "", userType: 1, cardId: ""),
        paymentType: "Transfer",
        recievers: recievers,
      ),
    );
  }
}
