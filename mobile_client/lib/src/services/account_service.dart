import 'dart:convert';

import 'package:futurebanking/src/config/config_api.dart';
import 'package:futurebanking/src/models/card_information.dart';
import 'package:futurebanking/src/models/payment_information.dart';
import 'package:futurebanking/src/models/reciever.dart';
import 'package:futurebanking/src/models/transaction_information.dart';
import 'package:futurebanking/src/utils/network_wrapper.dart';
import 'package:http/src/response.dart';

class AccountService {
  AccountService();

  static Future<List<CardInformation>> getCardsByUserId({required String userId}) async {
    List<CardInformation> cards = [];
    try {
      Response cardsResponse = await NetworkWrapper.get(
        path: Config.CARDS_CONTROLLER_URL,
        queryParameters: {
          "userId": userId,
        },
      );
      cards = List<CardInformation>.from(
          jsonDecode(cardsResponse.body).map((card) => CardInformation.fromJson(card)));
      return cards;
    } catch (e, s) {
      print("getCardsByUserId : Exception $e");
      print("getCardsByUserId : StackTrace $s");
    }
    return cards;
  }

  static Future<List<PaymentInformation>> getPaimentsByCardId({required String cardId}) async {
    List<PaymentInformation> payments = [];
    try {
      Response paimentsResponse = await NetworkWrapper.get(
        path: Config.TRANSACTION_CONTROLLER_URL + "/card",
        queryParameters: {
          "cardId": cardId,
        },
      );
      payments = List<PaymentInformation>.from(
          jsonDecode(paimentsResponse.body).map((card) => PaymentInformation.fromJson(card)));
      return payments;
    } catch (e, s) {
      print("getPaimentsByCardId : Exception $e");
      print("getPaimentsByCardId : StackTrace $s");
    }
    return payments;
  }

  static Future<List<Reciever>> getRecievers() async {
    List<Reciever> recievers = [];
    try {
      Response recieversResponse = await NetworkWrapper.get(
        path: Config.USERS_IDENTITY_CONTROLLER_URL,
        queryParameters: {
          "type": "1",
        },
      );
      print(recieversResponse.body);
      recievers = List<Reciever>.from(
          jsonDecode(recieversResponse.body).map((reciever) => Reciever.fromJson(reciever)));
      print(recievers);
      return recievers;
    } catch (e, s) {
      print("getRecievers : Exception $e");
      print("getRecievers : StackTrace $s");
    }
    return recievers;
  }

  static Future<void> sendMoneyRequest({required TransactionInformation body}) async {
    try {
      Response moneyResponse = await NetworkWrapper.post(
        path: Config.TRANSACTION_CONTROLLER_URL,
        headers: {
          "Content-Type": "application/json",
        },
        body: body.toJson(),
      );
      print(body.toJson());
      if (moneyResponse != null) {
        print("successful transaction");
      }
    } catch (e, s) {
      print("getRecievers : Exception $e");
      print("getRecievers : StackTrace $s");
    }
  }
}
