import 'dart:convert';

import 'package:futurebanking/src/config/config_api.dart';
import 'package:futurebanking/src/models/user.dart';
import 'package:futurebanking/src/utils/network_wrapper.dart';
import 'package:http/src/response.dart';

class UserService {
  UserService();

  static Future<User> authenticateUser({required String email, required String password}) async {
    User user = const User.initial();
    try {
      Response loginResponse = await NetworkWrapper.put(
        path: Config.USERS_IDENTITY_CONTROLLER_URL + "/login",
        queryParameters: {
          "Email": email,
          "Password": password,
        },
      );
      user = User.fromJson(jsonDecode(loginResponse.body));
      return user;
    } catch (e, s) {
      print("Exception $e");
      print("StackTrace $s");
    }
    return user;
  }
}
