// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_information.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardInformation _$CardInformationFromJson(Map<String, dynamic> json) =>
    CardInformation(
      type: json['type'] as String,
      id: json['id'] as String,
      holderName: json['holderName'] as String,
      expiryDate: DateTime.parse(json['expiryDate'] as String),
      number: json['number'] as String,
      cvv: json['cvv'] as String,
    );

Map<String, dynamic> _$CardInformationToJson(CardInformation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'number': instance.number,
      'cvv': instance.cvv,
      'expiryDate': instance.expiryDate.toIso8601String(),
      'holderName': instance.holderName,
      'type': instance.type,
    };
