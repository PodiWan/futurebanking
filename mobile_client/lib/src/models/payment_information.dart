import 'package:futurebanking/src/models/payment_type.dart';
import 'package:futurebanking/src/models/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'payment_information.g.dart';

@JsonSerializable()
class PaymentInformation {
  PaymentInformation({
    required this.currency,
    required this.day,
    required this.sender,
    required this.transferType,
    required this.amount,
    required this.cardType,
  });

  User sender;
  String transferType;
  int amount;
  String cardType;
  String currency;
  DateTime day;

  factory PaymentInformation.fromJson(Map<String, dynamic> json) =>
      _$PaymentInformationFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentInformationToJson(this);
}
