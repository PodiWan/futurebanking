import 'package:json_annotation/json_annotation.dart';

part "transaction_information.g.dart";

@JsonSerializable()
// {
// "senderCardId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
// "receiverCardId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
// "day": "2022-01-28T11:52:02.892Z",
// "amount": 0,
// "currency": 1,
// "transferType": 0
// }

class TransactionInformation {
  const TransactionInformation({
    required this.senderCardId,
    required this.receiverCardId,
    required this.day,
    required this.amount,
    required this.currency,
    required this.transferType,
  });

  final String senderCardId;
  final String receiverCardId;
  final DateTime day;
  final num amount;
  final int currency;
  final int transferType;

  factory TransactionInformation.fromJson(Map<String, dynamic> json) =>
      _$TransactionInformationFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionInformationToJson(this);
}
