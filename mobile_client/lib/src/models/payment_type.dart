import 'package:flutter/foundation.dart';

enum PaymentType { Transfer, Rental, Restaurantsndcafes, Sports }

extension PaymentTypeExtension on PaymentType {
  String get paymentToString {
    var name = describeEnum(this);
    return name.substring(0, 1).toUpperCase() + name.substring(1);
  }
}
