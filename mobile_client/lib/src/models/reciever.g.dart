// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reciever.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Reciever _$RecieverFromJson(Map<String, dynamic> json) => Reciever(
      id: json['id'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      email: json['email'] as String,
      avatarUrl: json['avatarUrl'] as String,
      userType: json['userType'] as num,
      cardId: json['cardId'] as String,
    );

Map<String, dynamic> _$RecieverToJson(Reciever instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'avatarUrl': instance.avatarUrl,
      'userType': instance.userType,
      'cardId': instance.cardId,
    };
