// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_information.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionInformation _$TransactionInformationFromJson(Map<String, dynamic> json) =>
    TransactionInformation(
      senderCardId: json['senderCardId'] as String,
      receiverCardId: json['receiverCardId'] as String,
      day: DateTime.parse(json['day'] as String),
      amount: json['amount'] as num,
      currency: json['currency'] as int,
      transferType: json['transferType'] as int,
    );

Map<String, dynamic> _$TransactionInformationToJson(TransactionInformation instance) =>
    <String, dynamic>{
      'senderCardId': instance.senderCardId,
      'receiverCardId': instance.receiverCardId,
      'day': instance.day.toIso8601String(),
      'amount': instance.amount,
      'currency': instance.currency,
      'transferType': instance.transferType,
    };
