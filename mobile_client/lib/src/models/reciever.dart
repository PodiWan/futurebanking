import 'package:json_annotation/json_annotation.dart';

part "reciever.g.dart";

// {
// "id": "7e10b5d8-8cf5-439c-b907-dc95f780e3fd",
// "firstName": "c",
// "lastName": "b",
// "email": "a@g.c",
// "avatarUrl": "https://st4.depositphotos.com/4329009/19956/v/600/depositphotos_199564354-stock-illustration-creative-vector-illustration-default-avatar.jpg",
// "userType": 1,
// "cardId": "00000000-0000-0000-0000-000000000000"
// },

@JsonSerializable()
class Reciever {
  const Reciever({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.avatarUrl,
    required this.userType,
    required this.cardId,
  });

  const Reciever.initial({
    this.id = "",
    this.firstName = "",
    this.lastName = "",
    this.email = "",
    this.avatarUrl = "",
    this.userType = -1,
    this.cardId = "",
  });

  final String id;
  final String firstName;
  final String lastName;
  final String email;
  final String avatarUrl;
  final num userType;
  final String cardId;

  Reciever copyWith({
    String? id,
    String? firstName,
    String? lastName,
    String? email,
    String? avatarUrl,
    num? userType,
    String? cardId,
  }) {
    return Reciever(
        id: id ?? this.id,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        email: email ?? this.email,
        avatarUrl: avatarUrl ?? this.avatarUrl,
        userType: userType ?? this.userType,
        cardId: cardId ?? this.cardId);
  }

  factory Reciever.fromJson(Map<String, dynamic> json) => _$RecieverFromJson(json);

  Map<String, dynamic> toJson() => _$RecieverToJson(this);
}
