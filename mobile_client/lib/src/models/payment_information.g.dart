// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_information.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentInformation _$PaymentInformationFromJson(Map<String, dynamic> json) =>
    PaymentInformation(
      currency: json['currency'] as String,
      day: DateTime.parse(json['day'] as String),
      sender: User.fromJson(json['sender'] as Map<String, dynamic>),
      transferType: json['transferType'] as String,
      amount: json['amount'] as int,
      cardType: json['cardType'] as String,
    );

Map<String, dynamic> _$PaymentInformationToJson(PaymentInformation instance) =>
    <String, dynamic>{
      'sender': instance.sender,
      'transferType': instance.transferType,
      'amount': instance.amount,
      'cardType': instance.cardType,
      'currency': instance.currency,
      'day': instance.day.toIso8601String(),
    };
