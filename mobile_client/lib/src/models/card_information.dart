import 'package:json_annotation/json_annotation.dart';

part 'card_information.g.dart';

@JsonSerializable()
class CardInformation {
  CardInformation({
    required this.type,
    required this.id,
    required this.holderName,
    required this.expiryDate,
    required this.number,
    required this.cvv,
  });

  final String id;
  final String number;
  final String cvv;
  final DateTime expiryDate;
  final String holderName;
  final String type;

  CardInformation copyWith({
    String? id,
    String? holderName,
    DateTime? expiryDate,
    String? number,
    String? cvv,
    String? type,
  }) =>
      CardInformation(
        id: id ?? this.id,
        holderName: holderName ?? this.holderName,
        expiryDate: expiryDate ?? this.expiryDate,
        number: number ?? this.number,
        cvv: cvv ?? this.cvv,
        type: type ?? this.type,
      );

  factory CardInformation.fromJson(Map<String, dynamic> json) => _$CardInformationFromJson(json);

  Map<String, dynamic> toJson() => _$CardInformationToJson(this);
}
