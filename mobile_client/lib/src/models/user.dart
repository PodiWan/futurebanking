import 'package:json_annotation/json_annotation.dart';

part "user.g.dart";

@JsonSerializable()
class User {
  const User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.avatarUrl,
    required this.type,
    required this.bankAccountIds,
  });

  const User.initial({
    this.id = "",
    this.firstName = "",
    this.lastName = "",
    this.email = "",
    this.avatarUrl = "",
    this.type = -1,
    this.bankAccountIds = const [],
  });

  final String id;
  final String firstName;
  final String lastName;
  final String email;
  final String avatarUrl;
  final num type;
  final List<String>? bankAccountIds;

  User copyWith({
    String? id,
    String? firstName,
    String? lastName,
    String? email,
    String? avatarUrl,
    num? type,
    List<String>? bankAccountIds,
  }) {
    return User(
        id: id ?? this.id,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        email: email ?? this.email,
        avatarUrl: avatarUrl ?? this.avatarUrl,
        type: type ?? this.type,
        bankAccountIds: bankAccountIds ?? this.bankAccountIds);
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
