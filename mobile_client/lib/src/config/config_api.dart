class Config {
  Config._();

  static const String API_URL = "6ba8-188-24-123-250.ngrok.io";
  static const String USERS_CONTROLLER_URL = "/api/users";
  static const String USERS_IDENTITY_CONTROLLER_URL = "/api/user-identity";
  static const String TRANSACTION_CONTROLLER_URL = "/api/transactions";
  static const String MORTGAGE_CONTROLLER_URL = "/api/mortgages";
  static const String CARDS_CONTROLLER_URL = "/api/cards";
}
