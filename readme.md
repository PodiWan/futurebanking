# Objectives

Banking app:
- user can:
    - view personalized spending statistics
    - track mortgages
    - request creation of a bank account
    - request a new card  
    - create deposit
    - check balance
    - have peer-to-peer transactions
    - see nearby atms
    - see nearby banks
    - seek for help from a bank officer
- bank officer can:
    - help a user requesting assistance
    - request contact details of a user
    - approve/reject creation of bank account
- bank admin can:
    - assign bank officers
- system:
    - handles requests from the users, bank officers and admins validates user roles for operations (meaning a simple user cannot do admin operations)
    - handles and ensures data persistance