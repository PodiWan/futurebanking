using System;
using System.Collections.Generic;
using fba.common.Enums;

namespace fba.common.Entities
{
    public class UserIdentity
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public UserType Type { get; set; }
        public string AvatarUrl { get; set; }
        public List<Guid> BankAccounts { get; set; }
        public string UserId { get; set; }
    }
}