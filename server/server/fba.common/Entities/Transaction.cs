using System;
using fba.common.Enums;

namespace fba.common.Entities
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public Guid SenderCardId { get; set; }
        public Guid ReceiverCardId { get; set; }
        public DateTime Day { get; set; }
        public double Amount { get; set; }
        public CurrencyType Currency { get; set; }
        public TransferType TransferType { get; set; }
    }
}