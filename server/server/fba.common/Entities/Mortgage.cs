using System;
using fba.common.Enums;

namespace fba.common.Entities
{
    public class Mortgage
    {
        public Guid Id { get; set; }
        public DateTime DueDate { get; set; }
        public double Amount { get; set; }
        public bool AutoPay { get; set; }
        public Guid BankAccountId { get; set; }
        public MortgageStatus Status { get; set; }
    }
}