using System;
using System.Collections.Generic;
using fba.common.Enums;

namespace fba.common.Entities
{
    public class BankAccount
    {
        public Guid Id { get; set; }
        public double Balance { get; set; }
        public CurrencyType Currency { get; set; }
        public List<Mortgage> Mortgages { get; set; }
        public List<Card> Cards { get; set; }
    }
}