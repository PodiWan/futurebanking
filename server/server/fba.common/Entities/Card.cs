using System;
using fba.common.Enums;

namespace fba.common.Entities
{
    public class Card
    {
        public Guid Id { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Guid BankAccountId { get; set; }
        public Guid UserId { get; set; }
        public CardType CardType { get; set; }
    }
}