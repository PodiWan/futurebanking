﻿using System;
using fba.common.Dtos.UserDtos;
using fba.common.Entities;
using fba.common.ViewModel.UserViewModels;

namespace fba.common.Extensions
{
    public static class UserExtensions
    {
        public static UserDto ToDto(this User user)
        {
            return new UserDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address
            };
        }

        public static CreateUserDto ToDto(this CreateUserViewModel user)
        {
            return new CreateUserDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address,
                Email = user.Email,
                Type = user.Type
            };
        }

        public static User ToUser(this CreateUserDto user)
        {
            return new User
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address
            };
        }
    }
}
