﻿using System;
using System.Collections.Generic;
using fba.common.Dtos.UserIdentityDtos;
using fba.common.Entities;

namespace fba.common.Extensions
{
    public static class UserIdentityExtensions
    {
        public static UserIdentityDto ToDto(this UserIdentity user)
        {
            if (user == null)
                return null;

            return new UserIdentityDto
            {
                Id = user.Id,
                Email = user.Email,
                PasswordHash = user.PasswordHash,
                Type = user.Type,
                AvatarUrl = user.AvatarUrl,
                BankAccounts = user.BankAccounts,
                UserId = user.UserId
            };
        }

        public static UserIdentity ToUserIdentity(this CreateUserIdentityDto dto)
        {
            if (dto == null)
                return null;

            return new UserIdentity
            {
                Id = Guid.NewGuid(),
                Email = dto.Email,
                PasswordHash = Guid.NewGuid().ToString(),
                Type = dto.Type,
                BankAccounts = new List<Guid>(),
                UserId = dto.UserId
            };
        }

        public static UserIdentity ToUserIdentity(this UserIdentityDto dto)
        {
            if (dto == null)
                return null;

            return new UserIdentity
            {
                Id = dto.Id,
                Email = dto.Email,
                PasswordHash = dto.PasswordHash,
                AvatarUrl = dto.AvatarUrl,
                Type = dto.Type,
                BankAccounts = dto.BankAccounts,
                UserId = dto.UserId
            };
        }
    }
}
