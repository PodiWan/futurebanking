﻿using fba.common.Dtos.MortgageDtos;
using fba.common.Entities;

namespace fba.common.Extensions
{
    public static class MortgageExtensions
    {
        public static MortgageDto ToDto(this Mortgage mortgage)
        {
            return new MortgageDto
            {
                Id = mortgage.Id,
                Amount = mortgage.Amount,
                AutoPay = mortgage.AutoPay,
                BankAccountId = mortgage.BankAccountId,
                DueDate = mortgage.DueDate,
                Status = mortgage.Status
            };
        }
    }
}
