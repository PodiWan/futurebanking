﻿using System;
using fba.common.Dtos.TransactionDtos;
using fba.common.Entities;
using fba.common.ViewModel;

namespace fba.common.Extensions
{
    public static class TransactionExtensions
    {
        public static TransactionDto ToDto(this Transaction transaction)
        {
            return new TransactionDto
            {
                Id = transaction.Id,
                Amount = transaction.Amount,
                Currency = transaction.Currency,
                Day = transaction.Day,
                ReceiverCardId = transaction.ReceiverCardId,
                SenderCardId = transaction.SenderCardId,
                TransferType = transaction.TransferType
            };
        }

        public static TransactionDto ToDto(this TransactionViewModel transaction)
        {
            return new TransactionDto
            {
                Id = Guid.NewGuid(),
                Amount = transaction.Amount,
                Currency = transaction.Currency,
                Day = transaction.Day,
                ReceiverCardId = transaction.ReceiverCardId,
                SenderCardId = transaction.SenderCardId,
                TransferType = transaction.TransferType
            };
        }

        public static Transaction ToTransaction(this TransactionDto dto)
        {
            return new Transaction
            {
                Id = dto.Id,
                Amount = dto.Amount,
                Currency = dto.Currency,
                Day = dto.Day,
                ReceiverCardId = dto.ReceiverCardId,
                SenderCardId = dto.SenderCardId,
                TransferType = dto.TransferType
            };
        }
    }
}
