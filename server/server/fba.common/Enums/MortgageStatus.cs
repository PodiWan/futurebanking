﻿namespace fba.common.Enums
{
    public enum MortgageStatus
    {
        InReview = 1,
        Approved,
        Rejected,
        None
    }
}
