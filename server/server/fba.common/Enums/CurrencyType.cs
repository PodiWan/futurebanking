namespace fba.common.Enums
{
    public enum CurrencyType
    {
        USD = 1,
        EUR,
        RON
    }
}