namespace fba.common.Enums
{
    public enum UserType
    {
        Customer = 1,
        BankOfficer,
        Admin
    }
}