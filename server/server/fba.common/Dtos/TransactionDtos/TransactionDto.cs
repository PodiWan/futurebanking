﻿using fba.common.Dtos.CardDtos;
using fba.common.Enums;
using System;

namespace fba.common.Dtos.TransactionDtos
{
    public class TransactionDto
    {
        public Guid Id { get; set; }
        public Guid SenderCardId { get; set; }
        public Guid ReceiverCardId { get; set; }
        public DateTime Day { get; set; }
        public double Amount { get; set; }
        public CurrencyType Currency { get; set; }
        public TransferType TransferType { get; set; } 
    }
}
