﻿using fba.common.Enums;

namespace fba.common.Dtos.UserDtos
{
    public class CreateUserDto
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public UserType Type { get; set; }
    }
}
