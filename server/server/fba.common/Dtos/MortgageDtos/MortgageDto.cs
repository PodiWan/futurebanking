﻿using System;
using fba.common.Enums;

namespace fba.common.Dtos.MortgageDtos
{
    public class MortgageDto
    {
        public Guid Id { get; set; }
        public DateTime DueDate { get; set; }
        public double Amount { get; set; }
        public bool AutoPay { get; set; }
        public Guid BankAccountId { get; set; }
        public MortgageStatus Status { get; set; }
    }
}
