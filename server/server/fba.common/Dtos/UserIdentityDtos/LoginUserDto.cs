﻿namespace fba.common.Dtos.UserIdentityDtos
{
    public class LoginUserDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
