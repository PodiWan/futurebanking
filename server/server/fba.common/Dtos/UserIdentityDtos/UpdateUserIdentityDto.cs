﻿using fba.common.Enums;

namespace fba.common.Dtos.UserIdentityDtos
{
    public class UpdateUserIdentityDto
    {
        public string PasswordHash { get; set; }
        public UserType Type { get; set; }
    }
}
