﻿using fba.common.Enums;
using System;
using System.Collections.Generic;

namespace fba.common.Dtos.UserIdentityDtos
{
    public class UserIdentityDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public UserType Type { get; set; }
        public string AvatarUrl { get; set; }
        public List<Guid> BankAccounts { get; set; }

        public string UserId { get; set; }
    }
}
