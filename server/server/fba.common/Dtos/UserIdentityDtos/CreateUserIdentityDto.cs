﻿using fba.common.Enums;

namespace fba.common.Dtos.UserIdentityDtos
{
    public class CreateUserIdentityDto
    {
        public string Email { get; set; }
        public UserType Type { get; set; }
        public string UserId { get; set; }
    }
}
