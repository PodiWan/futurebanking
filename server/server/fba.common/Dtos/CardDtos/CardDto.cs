﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fba.common.Dtos.CardDtos
{
    public class CardDto
    {
        public Guid Id { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Guid BankAccountId { get; set; }
        public Guid UserId { get; set; }
    }
}
