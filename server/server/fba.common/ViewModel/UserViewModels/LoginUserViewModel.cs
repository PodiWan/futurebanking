﻿namespace fba.common.ViewModel.UserViewModels
{
    public class LoginUserViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
