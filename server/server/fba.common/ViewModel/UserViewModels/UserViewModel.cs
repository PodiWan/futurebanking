﻿using System;
using fba.common.Enums;

namespace fba.common.ViewModel.UserViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get;set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public UserType UserType { get; set; }
    }
}
