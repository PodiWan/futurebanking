﻿using System;
using fba.common.Enums;

namespace fba.common.ViewModel
{
    public class TransactionViewModel
    {
        public Guid SenderCardId { get; set; }
        public Guid ReceiverCardId { get; set; }
        public DateTime Day { get; set; }
        public double Amount { get; set; }
        public CurrencyType Currency { get; set; }
        public TransferType TransferType { get; set; }
    }
}
