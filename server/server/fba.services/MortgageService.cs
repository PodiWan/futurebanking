﻿using System;
using fba.common.Dtos.MortgageDtos;
using fba.data;
using fba.interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fba.common.Entities;
using fba.common.Enums;
using fba.common.Extensions;

namespace fba.services
{
    public class MortgageService : IMortgageService
    {
        private DataContext _context;

        public MortgageService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<MortgageDto>> GetAllPagedAsync(int pageNumber, int pageSize)
        {
            var mortgages = await _context.Mortgages
                .Skip(pageNumber * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return mortgages.Any() ? mortgages.Select(mortgage => mortgage.ToDto()) : new List<MortgageDto>();
        }

        public async Task<MortgageDto> ReviewMortgage(Guid id, MortgageStatus status)
        {
            var mortgage = await _context.Mortgages
                .FirstOrDefaultAsync(mortgage => mortgage.Id == id);
            if (mortgage == null)
                return null;

            var updatedMortgage = new Mortgage
            {
                Id = id,
                Amount = mortgage.Amount,
                AutoPay = mortgage.AutoPay,
                BankAccountId = mortgage.BankAccountId,
                DueDate = mortgage.DueDate,
                Status = status
            };

            _context.Entry(mortgage).CurrentValues.SetValues(updatedMortgage);
            await _context.SaveChangesAsync();

            return mortgage.ToDto();
        }
    }
}
