﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fba.common.Dtos.UserDtos;
using fba.common.Dtos.UserIdentityDtos;
using fba.common.Enums;
using fba.common.Extensions;
using fba.data;
using fba.interfaces;
using Microsoft.EntityFrameworkCore;

namespace fba.services
{
    public class UserService : IUserService
    {
        private readonly DataContext _context;
        private readonly IEmailService _emailService;
        private readonly IUserIdentityService _userIdentityService;

        public UserService(
            DataContext context, 
            IEmailService emailService,
            IUserIdentityService userIdentityService)
        {
            _context = context;
            _emailService = emailService;
            _userIdentityService = userIdentityService;
        }

        public async Task<IEnumerable<UserDto>> GetPagedAsync(int pageNumber, int pageSize)
        {
            var users = await _context
                .Users
                .OrderBy(user => user.LastName)
                .ThenBy(user => user.FirstName)
                .Skip(pageNumber * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return users.Any() ? users.Select(user => user.ToDto()) : new List<UserDto>();
        }

        public async Task<UserDto> GetByIdAsync(string id)
        {
            var user = await _context
                .Users
                .FirstOrDefaultAsync(user => user.Id == id);

            return user?.ToDto();
        }

        public async Task<UserDto> AddAsync(CreateUserDto userDto)
        {
            var user = userDto.ToUser();

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            var userIdentity = await _userIdentityService.AddAsync(new CreateUserIdentityDto
            {
                Email = userDto.Email,
                Type = userDto.Type,
                UserId = user.Id
            });
            await _emailService.SendEmailWithPassword(userDto.Email, userDto.FirstName, userIdentity.PasswordHash);

            return user.ToDto();
        }

        public async Task<UserDto> DeleteAsync(string id)
        {
            var user = await _context
                .Users
                .FirstOrDefaultAsync(user => user.Id == id);

            if (user == null)
                return null;

            _context.Remove(user);
            await _context.SaveChangesAsync();

            return user.ToDto();
        }
    }
}
