﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fba.common.Dtos.UserIdentityDtos;
using fba.common.Entities;
using fba.common.Enums;
using fba.common.Extensions;
using fba.data;
using fba.interfaces;
using Microsoft.EntityFrameworkCore;

namespace fba.services
{
    public class UserIdentityService : IUserIdentityService
    {
        private readonly DataContext _context;

        public UserIdentityService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UserIdentityDto>> GetPagedAsync(int pageNumber, int pageSize)
        {
            var users = await _context
                .UserIdentities
                .OrderBy(user => user.Type)
                .ThenBy(user => user.Email)
                .Skip(pageNumber * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return users.Any() ? users.Select(user => user.ToDto()) : new List<UserIdentityDto>();
        }

        public async Task<UserIdentityDto> GetByIdAsync(Guid id)
        {
            var user = await _context
                .UserIdentities
                .OrderBy(user => user.Type)
                .ThenBy(user => user.Email)
                .Where(user => user.Id == id)
                .FirstOrDefaultAsync();

            return user?.ToDto();
        }

        public async Task<IEnumerable<UserIdentityDto>> GetByTypeAsync(UserType type)
        {
            var users = await _context
                .UserIdentities
                .OrderBy(user => user.Type)
                .ThenBy(user => user.Email)
                .Where(user => user.Type == type)
                .ToListAsync();

            return users.Any() ? users.Select(user => user.ToDto()).ToList() : new List<UserIdentityDto>();
        }
        public async Task<UserIdentityDto> AddAsync(CreateUserIdentityDto dto)
        {
            var user = dto.ToUserIdentity();

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return user?.ToDto();
        }

        public async Task<UserIdentityDto> UpdateAsync(Guid id, UpdateUserIdentityDto dto)
        {
            var existingUser = await _context.UserIdentities
                .FirstOrDefaultAsync(user => user.Id == id);
            if (existingUser == null)
                return null;

            var user = new UserIdentity
            {
                Id = existingUser.Id,
                Email = existingUser.Email,
                PasswordHash = dto.PasswordHash,
                Type = dto.Type,
                BankAccounts = existingUser.BankAccounts,
                UserId = existingUser.UserId
            };

            _context.Entry(existingUser).CurrentValues.SetValues(user);
            await _context.SaveChangesAsync();

            return existingUser.ToDto();
        }

        public async Task<UserIdentityDto> LoginAsync(LoginUserDto dto)
        {
            var user = await _context
                .UserIdentities
                .Where(user => user.Email == dto.Email && user.PasswordHash == dto.Password)
                .FirstOrDefaultAsync();

            return user.ToDto();
        }

        public async Task<UserIdentityDto> DeleteAsync(Guid id)
        {
            var user = await _context
                .UserIdentities
                .OrderBy(user => user.Type)
                .ThenBy(user => user.Email)
                .Where(user => user.Id == id)
                .FirstOrDefaultAsync();
            if (user == null)
                return null;

            _context.Remove(user);
            await _context.SaveChangesAsync();

            return user.ToDto();
        }

        public async Task<UserIdentityDto> NewPasswordAsync(Guid id)
        {
            var user = await GetByIdAsync(id);

            return await UpdateAsync(id, new UpdateUserIdentityDto
            {
                PasswordHash = Guid.NewGuid().ToString(),
                Type = user.Type
            });
        }
    }
}
