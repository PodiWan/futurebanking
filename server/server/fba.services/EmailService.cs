﻿using System.Threading.Tasks;
using fba.interfaces;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace fba.services
{
    public class EmailService : IEmailService
    {
        public async Task<bool> SendEmailWithPassword(string email, string name, string password)
        {
            var client = new SendGridClient("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");
            var from = new EmailAddress("noreply.futurebanking@gmail.com");
            var to = new EmailAddress(email, name);
            var message = MailHelper.CreateSingleEmail(from, to, "New Account Password", "", $"<div><h1>Hello, {name}<h1>! This is your new account password. Please " +
                $"change it after logging in.<br><p>{password}</p></div>");

            return (await client.SendEmailAsync(message)).IsSuccessStatusCode;
        }
    }
}
