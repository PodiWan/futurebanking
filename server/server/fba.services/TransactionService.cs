﻿using fba.common.Dtos.TransactionDtos;
using fba.common.Entities;
using fba.common.Extensions;
using fba.data;
using fba.interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fba.services
{
    public class TransactionService : ITransactionService
    {
        private readonly DataContext _context;

        public TransactionService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TransactionDto>> GetAllFromIdPagedAsync(int pageNumber, int pageSize, Guid cardId)
        {
            var response = await _context.Transactions
                .Where(transaction => transaction.ReceiverCardId == cardId)
                .ToListAsync();

            var secondaryResponse = await _context.Transactions
                .Where(transaction => transaction.SenderCardId == cardId)
                .ToListAsync();

            response.AddRange(secondaryResponse
                .Select(transaction => new Transaction
                {
                    Id = transaction.Id,
                    Amount = -transaction.Amount,
                    ReceiverCardId = transaction.ReceiverCardId,
                    SenderCardId = transaction.SenderCardId,
                    Currency = transaction.Currency,
                    Day = transaction.Day
                }).ToList());

            return response.Any() ? response.Select(transaction => transaction.ToDto()) : new List<TransactionDto>();
        }

        public async Task<IEnumerable<TransactionDto>> GetAllPagedAsync(int pageNumber, int pageSize)
        {
            var response = await _context
                .Transactions
                .Skip(pageNumber * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return response.Any() ? response.Select(transaction => transaction.ToDto()) : new List<TransactionDto>();
        }

        public async Task<TransactionDto> GetByIdAsync(Guid id)
        {
            var response = await _context.Transactions
                .FirstOrDefaultAsync(transaction => transaction.Id == id);

            return response?.ToDto();
        }

        public async Task<TransactionDto> AddAsync(TransactionDto dto)
        {
            var transaction = dto.ToTransaction();

            await _context.Transactions.AddAsync(transaction);
            await _context.SaveChangesAsync();

            return transaction?.ToDto();
        }
    }
}
