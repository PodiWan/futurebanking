﻿using System;
using fba.common.Dtos.TransactionDtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace fba.interfaces
{
    public interface ITransactionService
    {
        Task<IEnumerable<TransactionDto>> GetAllFromIdPagedAsync(int pageNumber, int pageSize, Guid senderCardId);
        Task<IEnumerable<TransactionDto>> GetAllPagedAsync(int pageNumber, int pageSize);
        Task<TransactionDto> GetByIdAsync(Guid transaction);
        Task<TransactionDto> AddAsync(TransactionDto transaction);
    }
}
