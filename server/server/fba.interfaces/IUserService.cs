﻿using System.Collections.Generic;
using System.Threading.Tasks;
using fba.common.Dtos.UserDtos;

namespace fba.interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> GetPagedAsync(int pageNumber, int pageSize);
        Task<UserDto> GetByIdAsync(string id);
        Task<UserDto> AddAsync(CreateUserDto userDto);
        Task<UserDto> DeleteAsync(string id);
    }
}
