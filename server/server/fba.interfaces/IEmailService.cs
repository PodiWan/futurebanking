﻿using System.Threading.Tasks;

namespace fba.interfaces
{
    public interface IEmailService
    {
        Task<bool> SendEmailWithPassword(string email, string name, string password);
    }
}
