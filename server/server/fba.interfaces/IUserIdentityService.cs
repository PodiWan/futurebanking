﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using fba.common.Dtos.UserIdentityDtos;
using fba.common.Enums;

namespace fba.interfaces
{
    public interface IUserIdentityService
    {
        Task<IEnumerable<UserIdentityDto>> GetPagedAsync(int pageNumber, int pageSize);
        Task<UserIdentityDto> GetByIdAsync(Guid id);
        Task<IEnumerable<UserIdentityDto>> GetByTypeAsync(UserType type);
        Task<UserIdentityDto> AddAsync(CreateUserIdentityDto dto);
        Task<UserIdentityDto> UpdateAsync(Guid id, UpdateUserIdentityDto dto);
        Task<UserIdentityDto> LoginAsync(LoginUserDto dto);
        Task<UserIdentityDto> DeleteAsync(Guid id);
        Task<UserIdentityDto> NewPasswordAsync(Guid id);
    }
}
