﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using fba.common.Dtos.MortgageDtos;
using fba.common.Enums;

namespace fba.interfaces
{
    public interface IMortgageService
    {
        Task<IEnumerable<MortgageDto>> GetAllPagedAsync(int pageNumber, int pageSize);
        Task<MortgageDto> ReviewMortgage(Guid id, MortgageStatus status);
    }
}
