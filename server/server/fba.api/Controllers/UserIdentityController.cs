﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using fba.common.Dtos.UserDtos;
using fba.common.Dtos.UserIdentityDtos;
using fba.common.ViewModel.UserViewModels;
using fba.interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using fba.common.Enums;

namespace fba.api.Controllers
{
    [Route("/api/user-identity")]
    public class UserIdentityController : ControllerBase
    {
        private readonly IUserIdentityService _userIdentityService;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;

        public UserIdentityController(
            IUserIdentityService userIdentityService,
            IUserService userService, 
            IEmailService emailService)
        {
            _userIdentityService = userIdentityService;
            _userService = userService;
            _emailService = emailService;
        }

        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetPagedAsync(int pageNumber = 0, int pageSize = 10)
        {
            var serviceResponse = await _userIdentityService.GetPagedAsync(pageNumber, pageSize);
            var response = new List<UserViewModel>();
            foreach (var userIdentityDto in serviceResponse)
            {
                var user = await _userService.GetByIdAsync(userIdentityDto.UserId);
                response.Add(new UserViewModel {
                    Id = userIdentityDto.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = userIdentityDto.Email,
                    AvatarUrl = userIdentityDto.AvatarUrl,
                    UserType = userIdentityDto.Type
                });
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("{type}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetByTypeAsync(UserType type)
        {
            var serviceResponse = await _userIdentityService.GetByTypeAsync(type);
            var response = new List<UserViewModel>();
            foreach (var userIdentityDto in serviceResponse)
            {
                var user = await _userService.GetByIdAsync(userIdentityDto.UserId);
                response.Add(new UserViewModel
                {
                    Id = userIdentityDto.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = userIdentityDto.Email,
                    AvatarUrl = userIdentityDto.AvatarUrl,
                    UserType = userIdentityDto.Type
                });
            }

            return Ok(response);
        }

        [HttpPut]
        [Route ("login")]
        [Produces("application/json")]
        public async Task<IActionResult> LoginAsync(LoginUserViewModel model)
        {
            var user = await _userIdentityService.LoginAsync(new LoginUserDto
            {
                Email = model.Email,
                Password = model.Password
            });

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [HttpPut]
        [Route("new-password")]
        [Produces("application/json")]
        public async Task<IActionResult> NewPasswordAsync(Guid id)
        {
            var user = await _userIdentityService.NewPasswordAsync(id);

            if (user == null)
                return BadRequest();

            var userFirstName = (await _userService.GetByIdAsync(user.UserId)).FirstName;
            await _emailService.SendEmailWithPassword(user.Email, userFirstName, user.PasswordHash);

            return Ok(user);
        }

        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> SignupAsync(CreateUserViewModel model)
        {
            var user = await _userService.AddAsync(new CreateUserDto
            {
                Id = model.Id,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address = model.Address,
                Type = model.Type
            });

            return Ok(user);
        }

        [HttpDelete]
        [Route("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var userIdentity = await _userIdentityService.GetByIdAsync(id);
            if (userIdentity == null)
                return NotFound();

            var user = await _userService.DeleteAsync(userIdentity.UserId);
            if (user == null)
                return BadRequest();

            return Ok(await _userIdentityService.DeleteAsync(id));
        }
    }
}