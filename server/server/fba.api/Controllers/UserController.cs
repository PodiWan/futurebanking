﻿using System.Threading.Tasks;
using fba.common.Extensions;
using fba.common.ViewModel.UserViewModels;
using fba.interfaces;
using Microsoft.AspNetCore.Mvc;

namespace fba.api.Controllers
{
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetPagedAsync(int pageNumber = 0, int pageSize = 10)
        {
            var response = await _userService.GetPagedAsync(pageNumber, pageSize);
            return Ok(response);
        }

        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> Register(CreateUserViewModel model)
        {
            var response = await _userService.AddAsync(model.ToDto());
            return Ok(response);
        }
    }
}