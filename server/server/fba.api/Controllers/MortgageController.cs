﻿using System;
using System.Threading.Tasks;
using fba.common.Enums;
using fba.interfaces;
using Microsoft.AspNetCore.Mvc;

namespace fba.api.Controllers
{
    [Route("/api/mortgages")]
    public class MortgageController : ControllerBase
    {
        private readonly IMortgageService _mortgageService;

        public MortgageController(IMortgageService mortgageService)
        {
            _mortgageService = mortgageService;
        }

        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllAsync(int pageNumber = 0, int pageSize = 10)
        {
            return Ok(await _mortgageService.GetAllPagedAsync(pageNumber, pageSize));
        }

        [HttpPut]
        [Route("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> ReviewMortgage(Guid id, MortgageStatus status)
        {
            return Ok(await _mortgageService.ReviewMortgage(id, status));
        }
    }
}