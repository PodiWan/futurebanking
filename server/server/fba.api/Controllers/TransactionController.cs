﻿using fba.common.Extensions;
using fba.common.ViewModel;
using fba.interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace fba.api.Controllers
{
    [Route("/api/transactions")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllAsync(int pageNumber = 0, int pageSize = 10)
        {
            return Ok(await _transactionService.GetAllPagedAsync(pageNumber, pageSize));
        }

        [HttpGet]
        [Route("card/{cardId}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllFromIdAsync(Guid cardId, int pageNumber = 0, int pageSize = 10)
        {
            return Ok(await _transactionService.GetAllFromIdPagedAsync(pageNumber, pageSize, cardId));
        }

        [HttpGet]
        [Route("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            var response = await _transactionService.GetByIdAsync(id);

            if (response == null)
                return NotFound();

            return Ok(response);
        }

        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> AddAsync(TransactionViewModel model)
        {
            var response = await _transactionService.AddAsync(model.ToDto());

            if (response == null)
                return BadRequest();

            return Ok(response);
        }
    }
}