using fba.data;
using fba.interfaces;
using fba.services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace fba.api.MiddlewareExtensions
{
    public static class ServiceExtensions
    {
        public static void DbOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("DbConnection")));
        }

        public static void InjectServices(this IServiceCollection services)
        {
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IMortgageService, MortgageService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserIdentityService, UserIdentityService>();
            services.AddTransient<ITransactionService, TransactionService>();
        }

        public static void SwaggerSetup(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "FutureBankingApp",
                    Version = "v1",
                    Description = ""
                });
            });
        }
    }
}