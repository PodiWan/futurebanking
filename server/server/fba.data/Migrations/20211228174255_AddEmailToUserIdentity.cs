﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fba.data.Migrations
{
    public partial class AddEmailToUserIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "UserIdentities",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "UserIdentities");
        }
    }
}
