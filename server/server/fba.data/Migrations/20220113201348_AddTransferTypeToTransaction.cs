﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fba.data.Migrations
{
    public partial class AddTransferTypeToTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_UserIdentities_UserIdentityId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_UserIdentityId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "UserIdentityId",
                table: "Cards");

            migrationBuilder.AddColumn<int>(
                name: "TransferType",
                table: "Transactions",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CardType",
                table: "Cards",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransferType",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "CardType",
                table: "Cards");

            migrationBuilder.AddColumn<Guid>(
                name: "UserIdentityId",
                table: "Cards",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cards_UserIdentityId",
                table: "Cards",
                column: "UserIdentityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_UserIdentities_UserIdentityId",
                table: "Cards",
                column: "UserIdentityId",
                principalTable: "UserIdentities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
