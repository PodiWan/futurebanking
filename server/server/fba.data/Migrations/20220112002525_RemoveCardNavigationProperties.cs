﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fba.data.Migrations
{
    public partial class RemoveCardNavigationProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Cards_ReceiverCardId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Cards_SenderCardId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_ReceiverCardId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_SenderCardId",
                table: "Transactions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Transactions_ReceiverCardId",
                table: "Transactions",
                column: "ReceiverCardId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_SenderCardId",
                table: "Transactions",
                column: "SenderCardId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Cards_ReceiverCardId",
                table: "Transactions",
                column: "ReceiverCardId",
                principalTable: "Cards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Cards_SenderCardId",
                table: "Transactions",
                column: "SenderCardId",
                principalTable: "Cards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
