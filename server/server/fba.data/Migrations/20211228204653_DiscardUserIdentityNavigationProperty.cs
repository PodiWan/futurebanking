﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fba.data.Migrations
{
    public partial class DiscardUserIdentityNavigationProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserIdentities_UserIdentityId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserIdentityId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserIdentityId",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "UserIdentities",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserIdentities");

            migrationBuilder.AddColumn<Guid>(
                name: "UserIdentityId",
                table: "Users",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserIdentityId",
                table: "Users",
                column: "UserIdentityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserIdentities_UserIdentityId",
                table: "Users",
                column: "UserIdentityId",
                principalTable: "UserIdentities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
