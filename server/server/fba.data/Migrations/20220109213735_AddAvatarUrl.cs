﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fba.data.Migrations
{
    public partial class AddAvatarUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankAccounts_UserIdentities_UserIdentityId",
                table: "BankAccounts");

            migrationBuilder.DropIndex(
                name: "IX_BankAccounts_UserIdentityId",
                table: "BankAccounts");

            migrationBuilder.DropColumn(
                name: "UserIdentityId",
                table: "BankAccounts");

            migrationBuilder.AddColumn<string>(
                name: "AvatarUrl",
                table: "UserIdentities",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<List<Guid>>(
                name: "BankAccounts",
                table: "UserIdentities",
                type: "uuid[]",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvatarUrl",
                table: "UserIdentities");

            migrationBuilder.DropColumn(
                name: "BankAccounts",
                table: "UserIdentities");

            migrationBuilder.AddColumn<Guid>(
                name: "UserIdentityId",
                table: "BankAccounts",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BankAccounts_UserIdentityId",
                table: "BankAccounts",
                column: "UserIdentityId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankAccounts_UserIdentities_UserIdentityId",
                table: "BankAccounts",
                column: "UserIdentityId",
                principalTable: "UserIdentities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
