﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fba.data.Migrations
{
    public partial class AddStatusToMortgages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Mortgages",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Mortgages");
        }
    }
}
