﻿// <auto-generated />
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using fba.data;

namespace fba.data.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20220109213735_AddAvatarUrl")]
    partial class AddAvatarUrl
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.13")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("fba.common.Entities.BankAccount", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<double>("Balance")
                        .HasColumnType("double precision");

                    b.Property<int>("Currency")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("BankAccounts");
                });

            modelBuilder.Entity("fba.common.Entities.Card", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<Guid>("BankAccountId")
                        .HasColumnType("uuid");

                    b.Property<DateTime>("ExpiryDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uuid");

                    b.Property<Guid?>("UserIdentityId")
                        .HasColumnType("uuid");

                    b.HasKey("Id");

                    b.HasIndex("BankAccountId");

                    b.HasIndex("UserIdentityId");

                    b.ToTable("Cards");
                });

            modelBuilder.Entity("fba.common.Entities.Mortgage", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<double>("Amount")
                        .HasColumnType("double precision");

                    b.Property<bool>("AutoPay")
                        .HasColumnType("boolean");

                    b.Property<Guid>("BankAccountId")
                        .HasColumnType("uuid");

                    b.Property<DateTime>("DueDate")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.HasIndex("BankAccountId");

                    b.ToTable("Mortgages");
                });

            modelBuilder.Entity("fba.common.Entities.Transaction", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<double>("Amount")
                        .HasColumnType("double precision");

                    b.Property<int>("Currency")
                        .HasColumnType("integer");

                    b.Property<DateTime>("Day")
                        .HasColumnType("timestamp without time zone");

                    b.Property<Guid>("ReceiverCardId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("SenderCardId")
                        .HasColumnType("uuid");

                    b.HasKey("Id");

                    b.HasIndex("ReceiverCardId");

                    b.HasIndex("SenderCardId");

                    b.ToTable("Transactions");
                });

            modelBuilder.Entity("fba.common.Entities.User", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<string>("Address")
                        .HasColumnType("text");

                    b.Property<string>("FirstName")
                        .HasColumnType("text");

                    b.Property<string>("LastName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("fba.common.Entities.UserIdentity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("AvatarUrl")
                        .HasColumnType("text");

                    b.Property<List<Guid>>("BankAccounts")
                        .HasColumnType("uuid[]");

                    b.Property<string>("Email")
                        .HasColumnType("text");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("text");

                    b.Property<int>("Type")
                        .HasColumnType("integer");

                    b.Property<string>("UserId")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("UserIdentities");
                });

            modelBuilder.Entity("fba.common.Entities.Card", b =>
                {
                    b.HasOne("fba.common.Entities.BankAccount", "BankAccount")
                        .WithMany("Cards")
                        .HasForeignKey("BankAccountId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("fba.common.Entities.UserIdentity", "UserIdentity")
                        .WithMany()
                        .HasForeignKey("UserIdentityId");

                    b.Navigation("BankAccount");

                    b.Navigation("UserIdentity");
                });

            modelBuilder.Entity("fba.common.Entities.Mortgage", b =>
                {
                    b.HasOne("fba.common.Entities.BankAccount", "BankAccount")
                        .WithMany("Mortgages")
                        .HasForeignKey("BankAccountId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("BankAccount");
                });

            modelBuilder.Entity("fba.common.Entities.Transaction", b =>
                {
                    b.HasOne("fba.common.Entities.Card", "ReceiverCard")
                        .WithMany()
                        .HasForeignKey("ReceiverCardId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("fba.common.Entities.Card", "SenderCard")
                        .WithMany()
                        .HasForeignKey("SenderCardId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ReceiverCard");

                    b.Navigation("SenderCard");
                });

            modelBuilder.Entity("fba.common.Entities.BankAccount", b =>
                {
                    b.Navigation("Cards");

                    b.Navigation("Mortgages");
                });
#pragma warning restore 612, 618
        }
    }
}
