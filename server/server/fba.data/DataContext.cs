﻿using fba.common.Entities;
using Microsoft.EntityFrameworkCore;

namespace fba.data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        //for ef migrations
        public DataContext() { }

        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Mortgage> Mortgages { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserIdentity> UserIdentities { get; set; }

        public DataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            optionsBuilder.UseNpgsql("Host=localhost;Database=futurebanking;Username=postgres;Password=postgres");
            return new DataContext(optionsBuilder.Options);
        }
    }
}