import { EnumDictionary } from "../types/EnumDictionary";
import MortgageStatus from "./MortgageStatus";

export const MortgageStatusToString: EnumDictionary<MortgageStatus, string> = {
    [MortgageStatus.InReview]: "in_review",
    [MortgageStatus.Approved]: "approved",
    [MortgageStatus.Rejected]: "rejected",
    [MortgageStatus.None]: ""
}

export const MortgageStatusFromString: EnumDictionary<string, MortgageStatus> = {
    "in_review": MortgageStatus.InReview,
    "approved": MortgageStatus.Approved,
    "rejected": MortgageStatus.Rejected,
    "": MortgageStatus.None
}