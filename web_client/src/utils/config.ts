export const Config = {
    API_URL: "https://localhost:5001",
    USERS_CONTROLLER_URL: "/api/users",
    USERS_IDENTITY_CONTROLLER_URL: "/api/user-identity",
    TRANSACTION_CONTROLLER_URL: "/api/transactions",
    MORTGAGE_CONTROLLER_URL: "/api/mortgages",
};  