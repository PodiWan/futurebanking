enum MortgageStatus {
    InReview = 1,
    Approved,
    Rejected,
    None
}

export default MortgageStatus;