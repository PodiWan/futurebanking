import { createSlice } from "@reduxjs/toolkit";

import {
  loginCaseReducer,
  logoutCaseReducer,
} from "./caseReducers";
import { userInitialState } from "./userState";

export const userSlice = createSlice({
  name: "user",
  initialState: userInitialState,
  reducers: {
    login: loginCaseReducer,
    logout: logoutCaseReducer,
  },
});

export const { login, logout } = userSlice.actions;
export default userSlice.reducer;
