import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import User from "../../models/User"

import { userInitialState, UserState } from "./userState";

export const logoutCaseReducer: CaseReducer<UserState> = (state) => {
  return userInitialState;
};

export const loginCaseReducer: CaseReducer<UserState, PayloadAction<User>> = (
  state,
  action
) => {
  return {
    isLoggedIn: true,
    userData: action.payload
  };
};