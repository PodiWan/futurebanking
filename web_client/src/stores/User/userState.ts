import User from "../../models/User";

export interface UserState {
    isLoggedIn: boolean;
    userData: User | undefined;
}

export const userInitialState: UserState = {
    isLoggedIn: false,
    userData: {
        id: 0,
        firstName: "",
        lastName: "",
        email: "",
        avatarUrl: "",
        type: 0,
    }
}