import {
  AnyAction,
  ThunkAction
} from "@reduxjs/toolkit";
import LoginUser from "../../models/LoginUser";
import { loginService, requestPasswordService } from "../../services/UserService";
import { RootState } from "../store";
import { login, logout } from "./slice";

export const loginUser =
  (loginUser: LoginUser): ThunkAction<void, RootState, null, AnyAction> =>
  async (dispatch) => {
    try {
      let response = await loginService(loginUser);
      console.log(response);
  
      if (response !== undefined) {
        dispatch(login(response));
      } else {
        console.log('{ "error": "could not log in" }');
      }
    } catch {
      console.log('{ "error": "could not authenticate" }');
    }
  };

export const logoutUser = (): ThunkAction<void, RootState, null, AnyAction> => 
  async (dispatch) => {
    dispatch(logout());
  }

export const requestPassword = (userId: string): ThunkAction<void, RootState, null, AnyAction> =>
  async () => {
    try {
      await requestPasswordService(userId);
  
    } catch {
      console.log('{ "error": "could not connect to server" }');
    }
  };