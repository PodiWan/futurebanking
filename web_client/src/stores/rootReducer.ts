import { combineReducers } from "@reduxjs/toolkit";

import bankOfficerReducer from "./BankOfficers/slice";
import mortgagesReducer from "./MortgageInformation/slice";
import transactionsReducer from "./TransactionInformation/slice";
import userReducer from "./User/slice";

export const rootReducer = combineReducers({
    userState: userReducer,
    mortgagesState: mortgagesReducer,
    transactionsState: transactionsReducer,
    bankOfficersState: bankOfficerReducer,
});