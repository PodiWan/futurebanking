import User from "../../models/User";

export interface BankOfficersState {
    officers: User[];
}

export const bankOfficersInitialState: BankOfficersState = {
    officers: [] as User[],
}