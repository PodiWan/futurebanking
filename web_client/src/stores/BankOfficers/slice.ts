import { createSlice } from "@reduxjs/toolkit";

import { setBankOfficersCaseReducer } from "./caseReducers";
import { bankOfficersInitialState } from "./bankOfficersState";

export const bankOfficer = createSlice({
  name: "user",
  initialState: bankOfficersInitialState,
  reducers: {
    setBankOfficers: setBankOfficersCaseReducer,
  },
});

export const { setBankOfficers } = bankOfficer.actions;
export default bankOfficer.reducer;
