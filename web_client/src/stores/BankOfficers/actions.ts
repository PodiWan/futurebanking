import {
  AnyAction,
  ThunkAction
} from "@reduxjs/toolkit";
import { deleteBankOfficerService, getBankOfficersService } from "../../services/BankOfficersService";
import { RootState } from "../store";
import { setBankOfficers } from "./slice";

export const getBankOfficers =
  (): ThunkAction<void, RootState, null, AnyAction> =>
  async (dispatch) => {
    try {
      var response = await getBankOfficersService();
  
      if (response !== undefined) {
        dispatch(setBankOfficers(response));
      } else {
        console.log('{ "error": "could not fetch bank officers" }');
      }
    } catch {
      console.log('{ "error": "could not connect to server" }');
    }
  };
  
export const deleteBankOfficer = (id: string): ThunkAction<void, RootState, null, AnyAction> =>
  async () => {
    try {
      var response = await deleteBankOfficerService(id);
  
      if (response === undefined) {
        console.log('{ "error": "could not fetch bank officers" }');
      }
    } catch {
      console.log('{ "error": "could not connect to server" }');
    }
  }