import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import User from "../../models/User"

import { BankOfficersState} from "./bankOfficersState";

export const setBankOfficersCaseReducer: CaseReducer<BankOfficersState, PayloadAction<User[]>> = (
  state,
  action
) => {
  return {
    officers: action.payload
  };
};