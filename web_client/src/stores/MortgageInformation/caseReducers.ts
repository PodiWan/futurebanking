import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import Mortgage from "../../models/MortgageInformation";

import { MortgagesState } from "./mortgagesState";

export const reviewCaseReducer: CaseReducer<MortgagesState, PayloadAction<Mortgage>> = 
    (state: MortgagesState, action: PayloadAction<Mortgage>) => {
        state.mortgages[state.mortgages.findIndex(mortgage => mortgage.id === action.payload.id)].status = action.payload.status;
    }

export const setMortgagesCaseReducer: CaseReducer<MortgagesState, PayloadAction<Mortgage[]>> = 
    (state: MortgagesState, action: PayloadAction<Mortgage[]>) => {
        state.mortgages = action.payload;
    };