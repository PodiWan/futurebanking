import Mortgage from "../../models/MortgageInformation";
import MortgageStatus from "../../utils/MortgageStatus";

export interface MortgagesState {
    mortgages: Mortgage[];
    currentMortgage: Mortgage | undefined;
}

export const mortgageInitialState: MortgagesState = {
    mortgages: [] as Mortgage[],
    currentMortgage: {
        id: "",
        bankAccountId: "",
        dueDate: "",
        amount: 0,
        currency: "",
        autoPay: false,
        status: MortgageStatus.None,
    }
};