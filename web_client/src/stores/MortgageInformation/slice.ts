import { createSlice } from "@reduxjs/toolkit";
import { reviewCaseReducer, setMortgagesCaseReducer } from "./caseReducers";
import { mortgageInitialState } from "./mortgagesState";

export const mortgageInformationSlice = createSlice({
    name: "mortgages",
    initialState: mortgageInitialState,
    reducers: {
        review: reviewCaseReducer,
        setMortgages: setMortgagesCaseReducer,
    }
});

export const { review, setMortgages } = mortgageInformationSlice.actions;
export default mortgageInformationSlice.reducer;