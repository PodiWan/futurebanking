import {
    AnyAction,
    ThunkAction,
  } from "@reduxjs/toolkit";
import ReviewMortgage from "../../models/ReviewMortgage";
import User from "../../models/User";
import { getMortgagesService, reviewMortgageService } from "../../services/MortgageService";

import MortgageStatus from "../../utils/MortgageStatus";
import { RootState } from "../store";
import { review, setMortgages } from "./slice";

export const reviewMortgage = 
    (id: string, status: MortgageStatus): ThunkAction<void, RootState, null, AnyAction> => 
        async (dispatch) => {
            try {
                console.log({ id: id, status: status } as ReviewMortgage);
                var response = await reviewMortgageService({ id: id, status: status } as ReviewMortgage);

                if (response) {
                    dispatch(review(response));
                } else {
                    console.log(" { error }: { could not review mortgage } ");
                }
            }
            catch(err) {
                console.log(err);
            }
        }


export const getMortgages = (): ThunkAction<void, RootState, null, AnyAction> =>
    async (dispatch) => {
        try {
            var response = await getMortgagesService();

            if (response) {
                console.log(response);
                dispatch(setMortgages(response));
            } else {
                console.log("{ error }: { could not fetch mortgages }")
            }
        }
        catch(err) {
            console.log(err);
        }
    }