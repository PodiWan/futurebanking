import {
    AnyAction,
    ThunkAction,
  } from "@reduxjs/toolkit";
import Transaction, { TransferType } from "../../models/Transaction";
import { getTransactionsService } from "../../services/TransactionService";

import { RootState } from "../store";
import { setTransactions } from "./slice";

export const getTransactions = (): ThunkAction<void, RootState, null, AnyAction> =>
    async (dispatch) => {
        try {
            var response = await getTransactionsService();

            if (response) {
                response = response.map(transaction => ({ ...transaction, transferType: TransferType[Number(transaction.transferType)] } as Transaction));
                dispatch(setTransactions(response));
            }
            else {
                console.log('{ "error": "could not fetch transactions" }');
            }
        }
        catch(err) {
            console.log(err);
        }
    }