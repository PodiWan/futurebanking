import Transaction from "../../models/Transaction";

export interface TransactionsState {
    transactions: Transaction[];
}

export const transactionInitialState: TransactionsState = {
    transactions: [] as Transaction[],
};