import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import Transaction from "../../models/Transaction";

import { TransactionsState } from "./transactionState";

export const setTransactionsCaseReducer: CaseReducer<TransactionsState, PayloadAction<Transaction[]>> = 
    (state: TransactionsState, action: PayloadAction<Transaction[]>) => {
        state.transactions = action.payload;
    };