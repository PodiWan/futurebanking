import { createSlice } from "@reduxjs/toolkit";
import { setTransactionsCaseReducer } from "./caseReducers";
import { transactionInitialState } from "./transactionState";

export const transactionSlice = createSlice({
    name: "mortgages",
    initialState: transactionInitialState,
    reducers: {
        setTransactions: setTransactionsCaseReducer,
    }
});

export const { setTransactions } = transactionSlice.actions;
export default transactionSlice.reducer;