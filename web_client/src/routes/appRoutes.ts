export const AppRoutes = {
    LOGIN: '/',
    DASHBOARD: '/dashboard',
    PROFILE: '/profile',
    MORTGAGES: '/mortgages',
    TRANSACTIONS: '/transactions',
    BANK_OFFICERS: '/bank-officers',
};