import { useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Routes as Switch } from "react-router-dom";
import BankOfficersPage from "../pages/BankOfficersPage/BankOfficersPage";
import Dashboard from "../pages/Dashboard/Dashboard";
import LoginPage from "../pages/LoginPage/LoginPage";
import MortgagesPage from "../pages/MortgagesPage/MortgagesPage";
import ProfilePage from "../pages/ProfilePage/ProfilePage";
import TransactionsPage from "../pages/TransactionsPage/TransactionsPage";
import { RootState } from "../stores/store";
import { AppRoutes } from "./appRoutes";

const LaunchRouter = () => {
    const userState = useSelector((state: RootState) => state.userState);

    return (
        <Router>
            <Switch>
                <Route path={AppRoutes.LOGIN} element={<LoginPage />} />
                {userState.isLoggedIn &&
                    <Route>
                        <Route path={AppRoutes.DASHBOARD} element={<Dashboard />} />
                        <Route path={AppRoutes.PROFILE} element={<ProfilePage />} />
                        <Route path={AppRoutes.MORTGAGES} element={<MortgagesPage />} />
                        <Route path={AppRoutes.TRANSACTIONS} element={<TransactionsPage />} />
                        <Route path={AppRoutes.BANK_OFFICERS} element={<BankOfficersPage/>} />
                    </Route>
                }
            </Switch>
        </Router>
    );
};

export default LaunchRouter;