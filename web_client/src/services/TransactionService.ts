import Transaction from "../models/Transaction";
import { Config } from "../utils/config";
import { axiosGetRequest } from "./api/axios";

export const getTransactionsService = async () =>
    axiosGetRequest<Transaction[]>(`${Config.API_URL}${Config.TRANSACTION_CONTROLLER_URL}`, "");