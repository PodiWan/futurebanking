import LoginUser from "../models/LoginUser";
import User from "../models/User";
import { Config } from "../utils/config";
import { axiosPutRequest } from "./api/axios";

export const loginService = async (loginUser: LoginUser) => 
    axiosPutRequest<string, User>(`${Config.API_URL}${Config.USERS_IDENTITY_CONTROLLER_URL}/login`,
        null,
        `?Email=${loginUser.email}&Password=${loginUser.password}`,
        "")

export const requestPasswordService = async (userId: string) =>
    axiosPutRequest<string, User>(`${Config.API_URL}${Config.USERS_IDENTITY_CONTROLLER_URL}/new-password`,
        null,
        `?id=${userId}`,
        ""
    );