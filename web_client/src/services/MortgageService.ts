import Mortgage from "../models/MortgageInformation";
import ReviewMortgage from "../models/ReviewMortgage";
import { Config } from "../utils/config";
import { axiosGetRequest, axiosPutRequest } from "./api/axios";

export const getMortgagesService = async () =>
    axiosGetRequest<Mortgage[]>(`${Config.API_URL}${Config.MORTGAGE_CONTROLLER_URL}`, "");

export const reviewMortgageService = async (mortgage: ReviewMortgage) =>
    axiosPutRequest<string, Mortgage>(
        `${Config.API_URL}${Config.MORTGAGE_CONTROLLER_URL}/${mortgage.id}`,
        null,
        `?status=${mortgage.status}`,
        ""
    );