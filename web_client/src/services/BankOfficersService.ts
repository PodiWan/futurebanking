import User from "../models/User";
import { Config } from "../utils/config";
import { axiosDeleteRequest, axiosGetRequest } from "./api/axios";

export const getBankOfficersService = async () =>
    axiosGetRequest<User[]>(`${Config.API_URL}${Config.USERS_IDENTITY_CONTROLLER_URL}`, "/2");

export const deleteBankOfficerService = async (id: string) =>
    axiosDeleteRequest<User>(`${Config.API_URL}${Config.USERS_IDENTITY_CONTROLLER_URL}/${id}`);