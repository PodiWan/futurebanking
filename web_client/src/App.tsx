import './App.css';
import LaunchRouter from './routes/LaunchRouter';

function App() {
  return <LaunchRouter />;
}

export default App;
