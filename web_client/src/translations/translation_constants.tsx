export const SupportedLanguages = {
    EN: "en",
    RO: "ro",
};

export const TranslationNamespaces = {
    LOGIN_PAGE: "login_page",
    DASHBOARD: "dashboard",
    NAVBAR: "navbar",
    TRANSACTIONS_PAGE: "transactions_page",
    MORTGAGES_PAGE: "mortgages_page",
    PROFILE_PAGE: "profile_page",
    BANK_OFFICERS: "bank_officers",
};