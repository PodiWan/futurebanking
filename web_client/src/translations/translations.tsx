import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { SupportedLanguages, TranslationNamespaces } from "./translation_constants";

const resources = {
  en: {
    login_page: require("./login_page/en.json"),
    dashboard: require("./dashboard/en.json"),
    navbar: require("./navbar/en.json"),
    transactions_page: require("./transactions_page/en.json"),
    mortgages_page: require("./mortgages_page/en.json"),
    profile_page: require("./profile_page/en.json"),
    bank_officers: require("./bank_officers/en.json"),
  },
  ro: {
    login_page: require("./login_page/ro.json"),
    dashboard: require("./dashboard/ro.json"),
    navbar: require("./navbar/ro.json"),
    transactions_page: require("./transactions_page/ro.json"),
    mortgages_page: require("./mortgages_page/ro.json"),
    profile_page: require("./profile_page/ro.json"),
    bank_officers: require("./bank_officers/ro.json"),
  },
};

i18n.use(LanguageDetector).init({
  resources,
  defaultNS: TranslationNamespaces.LOGIN_PAGE,
  fallbackLng: Object.values(SupportedLanguages.EN),
  supportedLngs: Object.values(SupportedLanguages),
  interpolation: { escapeValue: false },
});

export { SupportedLanguages, TranslationNamespaces };
export default i18n;
