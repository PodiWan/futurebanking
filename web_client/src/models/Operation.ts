export default interface Operation {
    label: string;
    route: string;
}