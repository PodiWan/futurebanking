import { EnumDictionary } from "../types/EnumDictionary"
export default interface Transaction {
    id: number;
    senderCardId: string;
    receiverCardId: string;
    transferType: string;
    amount: number;
    day: Date;
    currency: string;
};

export enum TransferType {
    Transfer,
    Rent,
    Shop
}