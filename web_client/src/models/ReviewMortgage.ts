import MortgageStatus from "../utils/MortgageStatus";

export default interface reviewMortgage {
    id: string;
    status: MortgageStatus;
}