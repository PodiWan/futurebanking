import { AppRoutes } from "../routes/appRoutes";
import Operation from "./Operation";
import Strategy from "./Strategy";

export default class InvalidUserStrategy implements Strategy {
    public renderDashboard(): Operation[] {
        return [
            { label: 'login', route: AppRoutes.LOGIN },
        ];
    }
}