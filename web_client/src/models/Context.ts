import Operation from "./Operation";
import Strategy from "./Strategy";

export default class Context {
    private strategy: Strategy;

    constructor(strategy: Strategy) {
        this.strategy = strategy;
    }

    public setStrategy(strategy: Strategy) {
        this.strategy = strategy;
    }

    public renderDashboard(): Operation[] { return this.strategy.renderDashboard() };
}