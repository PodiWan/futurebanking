export default interface User {
    id: number | undefined;
    firstName: string;
    lastName: string;
    email: string;
    avatarUrl: string;
    type: number;
}