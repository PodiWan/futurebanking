import { AppRoutes } from "../routes/appRoutes";
import Operation from "./Operation";
import Strategy from "./Strategy";

export default class BankOfficerStrategy implements Strategy {
    public renderDashboard(): Operation[] {
        return [
            { label: 'mortgages', route: AppRoutes.MORTGAGES },
            { label: 'transactions', route: AppRoutes.TRANSACTIONS },
        ];
    }
}