import MortgageStatus from "../utils/MortgageStatus";

export default interface Mortgage {
    id: string;
    bankAccountId: string;
    dueDate: string;
    amount: number;
    currency: string;
    autoPay: boolean;
    status: MortgageStatus;
}