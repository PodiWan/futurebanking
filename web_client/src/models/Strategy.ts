import Operation from "./Operation";

export default interface Strategy
{
    renderDashboard(): Operation[];
}