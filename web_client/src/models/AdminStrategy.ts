import { AppRoutes } from "../routes/appRoutes";
import Operation from "./Operation";
import Strategy from "./Strategy";

export default class AdminStrategy implements Strategy {
    public renderDashboard(): Operation[] {
        return [
            { label: 'mortgages', route: AppRoutes.MORTGAGES },
            { label: 'transactions', route: AppRoutes.TRANSACTIONS },
            { label: 'bank_officers', route: AppRoutes.BANK_OFFICERS }
        ];
    }
}