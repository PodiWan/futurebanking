export type EnumDictionary<T extends string | symbol | number, U> = {
    [k in T]: U;
};