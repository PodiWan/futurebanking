import { 
    Typography,
    TextField,
    Button,
    Paper,
    Snackbar,
    Alert,
    Stack,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
    useNavigate,
} from "react-router";
import LoginUser from "../../models/LoginUser";
import { AppRoutes } from "../../routes/appRoutes";
import { RootState, useAppDispatch } from "../../stores/store";
import { loginUser } from "../../stores/User/actions";

const LoginPage = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const userState = useSelector((state: RootState) => state.userState);
    var firstRender = true;

    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        if (userState.isLoggedIn) {
            navigate(AppRoutes.DASHBOARD)
        }
        else {
            if (!firstRender) {
                setOpenSnackbar(true);
            }
            else {
                firstRender = false;
            }
        }
    }, [userState.isLoggedIn]);

    const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>): void  => {
        setEmail(e.currentTarget.value)
    };

    const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>): void  => {
        setPassword(e.currentTarget.value)
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenSnackbar(false);
    };

    return(
        <Paper style={{"height": "100vh"}}>
            <Stack style={{"height": "100%"}} spacing={4} justifyContent="center" alignItems="center">
                <Typography 
                    variant="h3" 
                    component={"h3"}
                >
                    Future Banking
                    <Typography 
                        variant="h6" 
                        component={"h6"}
                    >
                        Admin Dashboard
                    </Typography>
                </Typography>
                <TextField 
                    label="email" 
                    variant="outlined"
                    onChange={handleEmailChange}
                    style={{"width": "20vw"}}
                ></TextField>
                <TextField 
                    label="password"
                    variant="outlined"
                    type="password"
                    onChange={handlePasswordChange}
                    style={{"width": "20vw"}}
                ></TextField>
                <Button variant="contained" style={{ "height": "5em", "width": "20vw"}}
                    onClick={() => {
                    dispatch(loginUser({ email: email, password: password } as LoginUser));
                }}>Login</Button>

                <Snackbar open={openSnackbar} autoHideDuration={10000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                        Error while authenticating! Please retry.
                    </Alert>
                </Snackbar>
            </Stack>
        </Paper>
    );
}

export default LoginPage;