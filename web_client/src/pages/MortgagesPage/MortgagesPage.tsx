import { 
    Alert,
    AlertProps,
    Paper,
    Snackbar,
    Stack,
    Typography,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import { GridRowId, useGridApiRef } from '@mui/x-data-grid';
import { DataGridPro, GridColumns } from '@mui/x-data-grid-pro';
import Mortgage from "../../models/MortgageInformation";
import { TranslationNamespaces } from "../../translations/translation_constants";
import Navbar from "../Dashboard/Navbar/Navbar";
import MortgageStatus from "../../utils/MortgageStatus";
import { MortgageStatusFromString } from "../../utils/MortgageStatusToString";
import { RootState, useAppDispatch } from "../../stores/store";
import { reviewMortgage } from "../../stores/MortgageInformation/actions";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getMortgages } from "../../stores/MortgageInformation/actions";
import { useCallback } from "react";

const MortgagesPage = () => {
    const { t } = useTranslation(TranslationNamespaces.MORTGAGES_PAGE);
    const dispatch = useAppDispatch();
    const [snackbar, setSnackbar] = useState<Pick<
        AlertProps,
        'children' | 'severity'
    > | null>(null);
    const mortgages = useSelector((state: RootState) => state.mortgagesState.mortgages);
    const apiRef = useGridApiRef();

    useEffect(() => {
        dispatch(getMortgages());
    }, []);

    const handleCloseSnackbar = () => setSnackbar(null);

    const columns: GridColumns = [
        { field: 'id', headerName: 'ID', width: 90 },
        { field: 'bankAccountId', headerName: t('bank_account'), width: 90 },
        { field: 'amount', headerName: t('amount'), type: 'number', width: 110, editable: false },
        { field: 'autoPay', headerName: t('currency'), type: 'number', width: 110, editable: false },
        { field: 'dueDate', headerName: t('date'), type: 'string', width: 250, editable: false },
        { field: 'status', headerName: t('status'), type: 'string', width: 250, editable: true },
      ];

    const reviewMortgageDispatchCall = useCallback((status: number, mortgage: Mortgage) => {
        dispatch(reviewMortgage(mortgage.id, status));
    }, []);

    const handleRowEditCommit = useCallback(
        async (id: GridRowId) => {
            const model = apiRef.current.getEditRowsModel(); // This object contains all rows that are being edited
            const newRow = model[id]; // The data that will be committed

            // The new value entered
            const status = newRow.status.value as string;
            const mortgage = mortgages.filter(row => row.id === id)[0];
            
            // Get the row old value before committing
            const oldRow = apiRef.current.getRow(id)!;

            try {
                // Make the HTTP request to save in the backend
                console.log(Number(status));
                reviewMortgageDispatchCall(Number(status), mortgage);
                setSnackbar({ children: 'Mortgage successfully saved', severity: 'success' });
            } catch (error) {
                setSnackbar({ children: 'Error while saving mortgage', severity: 'error' });
            // Restore the row in case of error
            apiRef.current.updateRows([oldRow]);
            }
        },
        [apiRef],
      );

    return(
        <Paper style={{"height": "100vh"}}>
            <Stack style={{"height": "100%"}} justifyContent="center" alignItems="center" spacing={5}>
                <Navbar/>
                <Typography variant="h2" component="h2">{t('title')}</Typography>
                <Stack
                    spacing={5} 
                    style={{"flex": "1", "overflow": "auto", "width": "100%"}}
                >
                    <DataGridPro apiRef={apiRef} rows={mortgages} columns={columns} editMode="row" onRowEditCommit={handleRowEditCommit} />
                    {!!snackbar && (
                        <Snackbar open onClose={handleCloseSnackbar} autoHideDuration={6000}>
                            <Alert {...snackbar} onClose={handleCloseSnackbar} />
                        </Snackbar>
                    )}
                </Stack>
            </Stack>
        </Paper>
    );
}

export default MortgagesPage;