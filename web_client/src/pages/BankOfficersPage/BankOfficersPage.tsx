import { Button, Paper, Stack, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { DataGridPro, GridColumns, GridApiRef, GridEventListener, GridEvents, GridToolbarContainer, useGridApiRef } from '@mui/x-data-grid-pro';
import { TranslationNamespaces } from "../../translations/translation_constants";
import Navbar from "../Dashboard/Navbar/Navbar";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "../../stores/store";
import { deleteBankOfficer, getBankOfficers } from "../../stores/BankOfficers/actions";
import { useEffect } from "react";
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import { GridActionsCellItem } from "@mui/x-data-grid";

interface EditToolbarProps {
    apiRef: GridApiRef;
  }
  
  const EditToolbar = (props: EditToolbarProps) => {
    const { apiRef } = props;
  
    const handleClick = () => {
      const id = apiRef.current.getRowsCount();
      apiRef.current.updateRows([{ id, isNew: true }]);
      apiRef.current.setRowMode(id, 'edit');
      // Wait for the grid to render with the new row
      setTimeout(() => {
        apiRef.current.scrollToIndexes({
          rowIndex: apiRef.current.getRowsCount() - 1,
        });
        apiRef.current.setCellFocus(id, 'first_name');
      });
    };
  
    return (
      <GridToolbarContainer>
        <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
          Add record
        </Button>
      </GridToolbarContainer>
    );
  }

const BankOfficersPage = () => {
    const apiRef = useGridApiRef();
    const { t } = useTranslation(TranslationNamespaces.BANK_OFFICERS);
    const dispatch = useAppDispatch();
    const bankOfficers = useSelector((state: RootState) => state.bankOfficersState.officers);

    useEffect(() => {
        dispatch(getBankOfficers());
    }, []);

    const handleRowFocusOut: GridEventListener<GridEvents.cellFocusOut> = (
        params,
        event,
      ) => {
        console.log("heyho");
    };

    const columns: GridColumns = [
        { field: 'id', headerName: 'ID', width: 90 },
        { field: 'firstName', headerName: t('first_name'), width: 120, editable: true },
        { field: 'lastName', headerName: t('last_name'), width: 120, editable: true },
        { field: 'email', headerName: t('email'), width: 200, editable: true },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            width: 100,
            cellClassName: 'actions',
            getActions: ({ id }) => {
              const isInEditMode = apiRef.current.getRowMode(id) === 'edit';
      
              if (isInEditMode) {
                return [
                    <GridActionsCellItem
                        icon={<SaveIcon />}
                        label="Save"
                            onClick={async () => {
                                const isValid = await apiRef.current.commitRowChange(id);
                                if (isValid) {
                                    apiRef.current.setRowMode(id, 'view');
                                    const row = apiRef.current.getRow(id);
                                    apiRef.current.updateRows([{ ...row, isNew: false }]);
                                }
                            }}
                            color="primary"
                  />,
                  <GridActionsCellItem
                        icon={<CancelIcon />}
                        label="Cancel"
                        className="textPrimary"
                        onClick={() => {
                            apiRef.current.setRowMode(id, 'view');
                            const row = apiRef.current.getRow(id);
                            if (row!.isNew) {
                                apiRef.current.updateRows([{ id, _action: 'delete' }]);
                            }
                        }}
                        color="inherit"
                  />,
                ];
              }
      
              return [
                <GridActionsCellItem
                    icon={<EditIcon />}
                    label="Edit"
                    className="textPrimary"
                    onClick={() => {
                        apiRef.current.setRowMode(id, 'edit');
                    }}
                    color="inherit"
                />,
                <GridActionsCellItem
                    icon={<DeleteIcon />}
                    label="Delete"
                    onClick={() => {
                      dispatch(deleteBankOfficer(id.toString()));
                      apiRef.current.updateRows([{ id, _action: 'delete' }]);
                    }}
                    color="inherit"
                />,
              ];
            },
          },
        ];

    return (
        <Paper style={{"height": "100vh"}}>
            <Stack style={{"height": "100%"}} justifyContent="center" alignItems="center" spacing={5}>
                <Navbar/>
                <Typography variant="h3" component="h1" >
                    {t('title')}
                </Typography>
                <Stack
                    spacing={5} 
                    style={{"flex": "1", "overflow": "auto", "width": "100%"}}
                >
                    <DataGridPro
                        apiRef={apiRef}
                        rows={bankOfficers}
                        editMode="row"
                        columns={columns}
                        onRowEditCommit={handleRowFocusOut}
                        components={{
                            Toolbar: EditToolbar,
                        }}
                        componentsProps={{
                            toolbar: { apiRef },
                        }}
                    />
                </Stack>
            </Stack>
        </Paper>
    );
}

export default BankOfficersPage;