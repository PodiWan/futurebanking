import { 
  Avatar,
  Menu, 
  MenuItem,
} from "@mui/material";
import * as React from "react";
import { TranslationNamespaces } from "../../../translations/translations";
import { useTranslation } from "react-i18next";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Tooltip from '@mui/material/Tooltip';
import {
  useNavigate,
} from "react-router";
import { AppRoutes } from "../../../routes/appRoutes";
import LanguageButton from "../LanguageButton/LanguageButton";
import { RootState, useAppDispatch } from "../../../stores/store";
import { logoutUser } from "../../../stores/User/actions";
import { useSelector } from "react-redux";

const Navbar = () => {
  const { t } = useTranslation([TranslationNamespaces.NAVBAR]);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const userState = useSelector((state: RootState) => state.userState);
  
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);
  
    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
      setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
      setAnchorElUser(null);
    };

    const settings = [
        { label: 'Dashboard', action: () => {navigate(AppRoutes.DASHBOARD)} },
        { label: 'Account', action: () => {navigate(AppRoutes.PROFILE)} },
        {
          label: 'Logout', action: () => {
            dispatch(logoutUser());
            navigate(AppRoutes.LOGIN);
        }
      },
    ];

    return (
      <AppBar position="static" style={{"backgroundColor": "#24252d"}}>
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
            >
              Future Banking
            </Typography>

            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
            >
              Future Banking
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}></Box>

            <LanguageButton />
            <Box sx={{ flexGrow: 0, p: '0 15px' }}>
                <Tooltip title={`${t('tooltip')}`}>
                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                        <Avatar 
                          alt={userState.userData?.email} 
                          src={"https://avatars.dicebear.com/api/identicon/6cc3f388-fdb1-4d2d-ba96-d4b51efe6d96.svg"} />
                    </IconButton>
                </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                {settings.map((setting) => (
                    <MenuItem key={setting.label} onClick={setting.action}>
                        <Typography textAlign="center">{t(setting.label)}</Typography>
                    </MenuItem>
                ))}
              </Menu>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    );
  };

export default Navbar;