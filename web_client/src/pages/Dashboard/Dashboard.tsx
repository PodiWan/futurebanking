import { 
    Button,
    Paper, 
    Stack,
    Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import AdminStrategy from "../../models/AdminStrategy";
import BankOfficerStrategy from "../../models/BankOfficerStrategy";
import InvalidUserStrategy from "../../models/InvalidUserStrategy";
import Context from "../../models/Context";
import Operation from "../../models/Operation";
import { RootState } from "../../stores/store";
import { TranslationNamespaces } from "../../translations/translations";
import Navbar from "./Navbar/Navbar";

var context = new Context(new BankOfficerStrategy());

const Dashboard = () => {
    const { t } = useTranslation([TranslationNamespaces.DASHBOARD]);
    const navigate = useNavigate();
    const [operations, setOperations] = useState([] as Operation[]);
    const userState = useSelector((state: RootState) => state.userState);

    useEffect(() => {
        switch (userState.userData?.type) {
            case 1: { context.setStrategy(new InvalidUserStrategy()); break;}
            case 2: { context.setStrategy(new BankOfficerStrategy()); break;}
            case 3: { context.setStrategy(new AdminStrategy()); break;}
        }
        setOperations(context.renderDashboard());
    }, [context]);

    return (
        <Paper style={{"height": "100vh"}}>
            <Stack style={{"flex": "1", "overflow": "auto", }} justifyContent="center" alignItems="center" spacing={10}>
                <Navbar/>
                <Typography variant="h3" component="h1" >
                    {t("sample_text")}
                </Typography>
                <Stack
                    spacing={5}
                    justifyContent="center"
                    style={{"width": "50vw"}}
                >
                    {operations.map((operation) => {
                        return <Button key={operation.label} onClick={() => {
                            navigate(operation.route);
                        }} variant="contained" style={{ "height": "5em" }}>
                            {t(operation.label)}
                        </Button>
                    })}
                </Stack>
            </Stack>
        </Paper>
    );
};

export default Dashboard;