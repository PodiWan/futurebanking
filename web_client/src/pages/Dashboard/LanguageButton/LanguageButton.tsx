import { 
    Button,
    Menu, 
    MenuItem,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import * as React from "react";
import i18n, { SupportedLanguages } from "../../../translations/translations";
import { changeLanguage } from "i18next";

const LanguageButton = () => {
    const [currentLanguage, setCurrentLanguage] = React.useState(i18n.language);
    const [anchorElement, setAnchorElement] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorElement);

    const openMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElement(event.currentTarget);
    };
    const closeMenu = () => {
        setAnchorElement(null);
    };
    const handleLanguageChange = (lang: string) => {
        changeLanguage(lang);
        setCurrentLanguage(lang);
        closeMenu();
    };

    const supportedLanguages = () => Object.values(SupportedLanguages).map((l) => (
        <MenuItem 
            key={l}
            value={l}
            onClick={() => handleLanguageChange(l)}
        >
            {l.toUpperCase()}
        </MenuItem>
    ));

    return(
        <>
            <Button variant="contained"
                onClick={openMenu}
                endIcon={<KeyboardArrowDownIcon />}
            >
                {currentLanguage}
            </Button>
            <Menu
                anchorEl={anchorElement}
                open={open}
                onClose={closeMenu}
            >
                {supportedLanguages()}
            </Menu>
        </>
    );
}

export default LanguageButton;