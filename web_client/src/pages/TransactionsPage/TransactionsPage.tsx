import { 
    Paper, 
    Stack,
    Typography,
} from "@mui/material";
import { DataGridPro, GridColumns } from '@mui/x-data-grid-pro';
import { useTranslation } from "react-i18next";
import Navbar from "../Dashboard/Navbar/Navbar";
import { TranslationNamespaces } from "../../translations/translation_constants";
import { RootState, useAppDispatch } from "../../stores/store";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { getTransactions } from "../../stores/TransactionInformation/actions";

const TransactionsPage = () => {
    const { t } = useTranslation(TranslationNamespaces.TRANSACTIONS_PAGE);
    const dispatch = useAppDispatch();
    const transactions = useSelector((state: RootState) => state.transactionsState.transactions);

    useEffect(() => {
        dispatch(getTransactions());
    }, []);

    const columns: GridColumns = [
        { field: 'id', headerName: 'ID', width: 90 },
        { field: 'senderCardId', headerName: t('sender'), width: 90 },
        { field: 'receiverCardId', headerName: t('target'), width: 90 },
        { field: 'transferType', headerName: t('transfer_type'), width: 110 },
        { field: 'amount', headerName: t('amount'), type: 'number', width: 110, editable: false },
        { field: 'currency', headerName: t('currency'), type: 'number', width: 110, editable: false },
        { field: 'cardType', headerName: t('card_type'), type: 'string', width: 150, editable: false },
        { field: 'day', headerName: t('date'), type: 'string', width: 250, editable: false },
      ];

    return(
        <Paper style={{"height": "100vh"}}>
            <Stack style={{"height": "100%"}} justifyContent="center" alignItems="center" spacing={5}>
                <Navbar/>
                <Typography variant="h2" component="h2">{t('title')}</Typography>
                <Stack
                    spacing={5} 
                    style={{"flex": "1", "overflow": "auto", "width": "100%"}}
                >
                    <DataGridPro rows={transactions} columns={columns} />
                </Stack>
            </Stack>
        </Paper>
    );
}

export default TransactionsPage;
