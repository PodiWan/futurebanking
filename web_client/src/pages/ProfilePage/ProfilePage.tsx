import { 
    Alert,
    Button,
    Snackbar,
    Stack, 
    Typography 
} from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { AppRoutes } from "../../routes/appRoutes";
import { RootState, useAppDispatch } from "../../stores/store";
import { requestPassword } from "../../stores/User/actions";
import { logout } from "../../stores/User/slice";
import { TranslationNamespaces } from "../../translations/translation_constants";
import Navbar from "../Dashboard/Navbar/Navbar";

const ProfilePage = () => {
    const { t } = useTranslation(TranslationNamespaces.PROFILE_PAGE);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [id, setId] = useState('');
    const userState = useSelector((state: RootState) => state.userState);

    useEffect(() => {
        var id = 0;
        if (userState.userData !== undefined) {
            id = userState.userData.id ?? 0;
        }

        setId(id.toString());
    }, [userState]);

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenSnackbar(false);
    };

    return(
        <Stack style={{"flex": "1", "overflow": "auto", }} justifyContent="center" alignItems="center" spacing={10}>
            <Navbar/>
            <Typography variant="h3" component="h1" >
                {t('title')}
            </Typography>
            <Button variant="contained" style={{ "height": "5em", "width": "50vw" }}
                onClick={() => {
                    dispatch(requestPassword(id));
                    setOpenSnackbar(true);
                    dispatch(logout());
                    navigate(AppRoutes.LOGIN);
                }}
            >
                {t('password_request')}
            </Button>

            <Snackbar open={openSnackbar} autoHideDuration={10000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    {t('request_issued')}
                </Alert>
            </Snackbar>
        </Stack>
    );
}

export default ProfilePage;