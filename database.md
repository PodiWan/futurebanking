# Database structure:
User:
- social security number
- first name
- last name
- phone number

User Identity:
- id
- social security number
- image URL
- email
- password
- creation_date

Bank Employee:
- id
- first name
- last name
- image URL
- email
- password
- role

Payment:
- id
- from id
- to id
- amount
- date
- type

Company:
- id
- name
- image URL

Card:
- id
- account id
- card type
- card number
- expiry date
- cvv code

Bank account:
- id
- user id
- currency
- amount

Mortgage:
- id
- account id
- approver id
- term
- amount
- automatic payment